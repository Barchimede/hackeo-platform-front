$(document).ready(function() {
    if (!localStorage.getItem("hadTuto")) {
        localStorage.setItem("hadTuto", false);
    }

    var hadTuto = localStorage.getItem("hadTuto");

    if (hadTuto == "false") {
        $(".tutoModal").addClass("is-active");
        $(".welcoming").addClass("actif");
    } else {
        $(".tutoModal").removeClass("is-active");
        $(".welcoming").removeClass("actif");
        $(".old").addClass("actif");
    }

    // Modal import own file
    $(".importFile").on("click", function() {
        $(".editor-import-modal").addClass("is-active");
    });

    $(".modal-close").on("click", function() {
        $(".editor-import-modal").removeClass("is-active");
        $(".btn-header-editor").removeClass("is-active");
    });

    $(".header-quit").on("click", function() {
        $(".editor-3d-content").css("display", "none");
        $(".btn-header-editor").css("display", "none");
        $(".editor-import-modal").removeClass("is-active");
    });

    $(".nav-tabs a").on("click", function() {
        $(".nav-tabs a").removeClass("actif");
        $(this).addClass("actif");

        $(".import-block").removeClass("actif");

        $("#importBtn").removeClass("model3d-import");
        $("#importBtn").removeClass("image-import");

        if (this.classList[0] == "import-image") {
            $(".import-block.image").addClass("actif");
            $("#importBtn").addClass("image-import");
        } else {
            $(".import-block.3d").addClass("actif");
            $("#importBtn").addClass("model3d-import");
        }
    });

    $(".btn-import-cancel").on("click", function() {
        $(".editor-import-modal").removeClass("is-active");
    });

    $(".editor-import-modal .modal-background").on(
        "click",
        function() {
            $(".editor-import-modal").removeClass("is-active");
        }
    );

    $(".tuto-modal").on("click", function() {
        $(".tutoModal").addClass("is-active");
    });

    $(".tutoModal .modal-background").on("click", function() {
        $(".tutoModal").removeClass("is-active");
        localStorage.setItem("hadTuto", true);
    });

    $(".tutoModal .modal-close").on("click", function() {
        $(".tutoModal").removeClass("is-active");
        localStorage.setItem("hadTuto", true);
    });
});