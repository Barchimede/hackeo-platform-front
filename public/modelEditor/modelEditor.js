$(document).ready(function() {
    console.log(
        "%c EDITOR 3D V2.1.4 ",
        "background: linear-gradient(to right, #da4453, #89216b); padding:5px; font-size: 10px; color: #ffffff"
    );

    var camera, scene, renderer, control;
    const API_KEY = "AIzaSyClD_Wh0dW9R3J5OCf2enPHc_pJm3vDgWE";
    const maxComplexity = "MEDIUM";
    const polyModelNumber = 100;

    let initialScale;
    let initialRotation;
    let initialPosition;

    let rotationBeforeInfinite;
    let previousRot = "None";
    let rotX = false;
    let rotY = false;
    let rotZ = false;

    let translationBeforeBoeing;

    let deltaTime = 0.05;

    /* Different camera positions */
    var southE = new THREE.Vector3(-5, 6, -5);
    var southO = new THREE.Vector3(5, 6, -5);
    var northE = new THREE.Vector3(-5, 6, 5);
    var northO = new THREE.Vector3(5, 6, 5);
    var up = new THREE.Vector3(0, 10, -5);

    var waypointID = parseInt($("#waypointID").text());
    var polyJson = $(".poly-json").text();

    var isDemo = false;
    var isSearching = false;
    var fileName;
    var companyName = $("#companyName").text();
    var isLoaded = false;
    var extension, typeFile;
    if ($(".markerLink").length > 0) {
        var markerTexturePath = "https://" + $(".markerLink").text();
    }

    var colorSelected;
    var haveColor;
    var newHex;
    var animationSelected;
    var isBoxAnimation = false;
    var isBoxColor = false;
    var animationSelectedDiv;

    var planeleft;
    var planeRight;
    var planeBack;
    var planeTop;
    var planeArrow;
    var planeUser;

    var resultFile;
    var resultFileName;
    var isImage = false;
    var isOldPoly = false;

    var frontRot = new THREE.Quaternion(
        -0.2706650854285702,
        0.1334204293289654,
        0.03790798355574941,
        0.9526291961463055
    );
    var upRot = new THREE.Quaternion(
        -0.7002651067796246,
        0,
        0,
        0.7002790544114057
    );

    renderer = new THREE.WebGLRenderer();
    //renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize(viewer.offsetWidth, viewer.offsetHeight);
    // renderer.setSize( window.innerWidth, window.innerHeight );
    viewer.appendChild(renderer.domElement);

    camera = new THREE.PerspectiveCamera(
        70,
        viewer.offsetWidth / viewer.offsetHeight,
        0.01,
        100
    );
    camera.position.set(southE.x, southE.y, southE.z);
    camera.rotation.set(frontRot.x, frontRot.y, frontRot.z);
    var camStatus = "SouthE";
    camera.lookAt(new THREE.Vector3(0, 1.5, 0));

    scene = new THREE.Scene();
    scene.add(new THREE.GridHelper(10, 10));
    scene.background = new THREE.Color(0x222222);

    var ambient = new THREE.HemisphereLight(0xbbbbff, 0x886666, 0.75);
    ambient.position.set(-0.5, 0.75, -1);
    scene.add(ambient);

    var light = new THREE.DirectionalLight(0xffffff, 0.5);
    light.position.set(-5, 5, 0);
    scene.add(light);

    var container = new THREE.Group();
    scene.add(container);

    control = new THREE.TransformControls(camera, renderer.domElement);
    control.addEventListener("change", render);

    /// Marker initialization
    var textureMarker, mat, texture;
    textureMarker = new THREE.TextureLoader().load(
        "../../modelEditor/marker.jpg",
        function() {
            render();
        }
    );
    // texture = new THREE.TextureLoader().load("", function () {render()});
    textureMarker.wrapS = THREE.RepeatWrapping;
    textureMarker.wrapT = THREE.RepeatWrapping;

    textureMarker.repeat.set(1, 1);

    mat = new THREE.MeshBasicMaterial({ map: textureMarker });

    var geometry = new THREE.PlaneGeometry(5, 5);

    var plane = new THREE.Mesh(geometry, mat);
    plane.material.side = THREE.DoubleSide;
    plane.rotateX(1.5708);
    // plane.rotateZ(1.5708*2);
    plane.rotateY(1.5708 * 2);
    plane.position.y += 0.01;
    scene.add(plane);

    var objects = new Map();
    var gifs = [];

    var actualObject;
    var actualUrl;
    var actualObjSize;
    var actualModel;
    var offset;

    var mouseInitX;
    var mouseInitY;

    //Box properties
    var boxActivated = false;
    var boxColor = "#777777";
    var boxAnimation = "NONE";

    var infosType;
    var infos;
    var jsonModel;
    var idBack;

    init();
    render();

    function searchPoly(keywords, onLoad) {
        var url = `https://poly.googleapis.com/v1/assets?keywords=${keywords}&format=OBJ&key=${API_KEY}&maxComplexity=${maxComplexity}&pageSize=${polyModelNumber}`;

        var request = new XMLHttpRequest();
        request.open("GET", url, true);
        request.addEventListener("load", function(event) {
            onLoad(JSON.parse(event.target.response));
        });
        request.send(null);
    }

    $(".input-poly")
        .focusin(function() {
            isSearching = true;
        })
        .focusout(function() {
            isSearching = false;
        });

    if (
        window.location.href.indexOf("127.0.0.1") > -1 ||
        window.location.href.indexOf("localhost") > -1
    ) {
        var urlVars = document.URL.substring(0, document.URL.lastIndexOf("/"));
        var urlSplited = urlVars.split("/");
        urlSplited.splice(-2, 5);
        var urlPlatform = urlSplited.join("/");
    } else if (window.location.href.indexOf("dev.hackeo.pro") > -1) {
        var urlPlatform = "https://dev.hackeo.pro";
    } else if (
        window.location.href.indexOf("www.hackeo.pro") > -1 ||
        window.location.origin == "https://hackeo.pro"
    ) {
        var urlPlatform = "https://www.hackeo.pro";
    }

    function createImage(asset) {
        // console.log("asset " + asset);
        var image = document.createElement("img");
        image.src = asset.thumbnail.url;
        image.style.width = "23%";
        image.className = "results-img";
        // image.style.height = '75px';

        var format = asset.formats.find(format => {
            return format.formatType === "OBJ";
        });

        if (format !== undefined) {
            image.onclick = function() {
                if (container.children.length > 3) {
                    // toastr.error("Vous avez dépassé la limite de 3 modèles");
                    $(".limit-model").addClass("actif");

                    setTimeout(function() {
                        $(".limit-model").removeClass("actif");
                    }, 5000);

                    $(".delete").on("click", function() {
                        $(".limit-model").removeClass("actif");
                    });
                    return;
                }

                // Remove previous results

                document.getElementById("spinner").hidden = false;
                // while ( container.children.length ) {
                //   container.remove( container.children[ 0 ] );
                // }

                asset_name.textContent = asset.displayName;
                asset_author.textContent = asset.authorName;

                asset_delete_name.textContent = asset.displayName;

                var obj = format.root;
                obj.url = obj.url.replace(
                    "poly.googleapis.com",
                    "vrassets-pa.googleusercontent.com"
                );
                var mtl = format.resources.find(resource => {
                    return resource.url.endsWith("mtl");
                });
                mtl.url = mtl.url.replace(
                    "poly.googleapis.com",
                    "vrassets-pa.googleusercontent.com"
                );

                var path = obj.url.slice(0, obj.url.indexOf(obj.relativePath));

                // console.log(path);
                actualUrl = path;
                var loader = new THREE.MTLLoader();
                loader.setCrossOrigin(true);
                loader.setMaterialOptions({ ignoreZeroRGBs: true });
                loader.setTexturePath(path);
                loader.load(mtl.url, function(materials) {
                    var loader = new THREE.OBJLoader();
                    loader.setMaterials(materials);
                    loader.load(obj.url, function(object) {
                        box = new THREE.Box3();
                        box.setFromObject(object);

                        var center = box.getCenter();
                        // center.y = box.min.y;
                        object.position.sub(center);
                        //object.scale.multiplyScalar(5);

                        var scaler = new THREE.Group();
                        scaler.add(object);
                        // console.log(box.getSize());
                        scaler.scale.setScalar(8 / box.getSize().length());

                        actualModel = object;
                        actualObject = scaler;
                        // offset = actualModel.getWorldPosition().sub(actualObject.getWorldPosition());
                        // console.log("Offset: " + offset);
                        // actualModel.position.sub(offset);
                        // actualObject.position.add(offset);
                        initialPosition = new THREE.Vector3().copy(
                            scaler.position
                        );
                        initialScale = new THREE.Vector3().copy(scaler.scale);
                        initialRotation = new THREE.Quaternion().copy(
                            scaler.quaternion
                        );

                        container.add(scaler);

                        RotNone();
                        translationBeforeBoeing = initialPosition;
                        BoeingNone();

                        var value = {
                            url: actualUrl,
                            author: asset_name.textContent,
                            name: asset_author.textContent,
                            scaler: scaler,
                            initPos: initialPosition,
                            initScale: initialScale,
                            initRot: initialRotation,
                            RotBeforeInf: rotationBeforeInfinite,
                            previousRot: previousRot,
                            rotX: rotX,
                            rotY: rotY,
                            rotZ: rotZ,
                            speed: 0,
                            extension: "POLY",
                            thumbUrl: asset.thumbnail.url,
                            isBoeing: isBoeing,
                            transBeforeBoeing: translationBeforeBoeing,
                            isOld: false,
                            idBack: null,
                            box: box
                        };
                        objects.set(object, value);

                        control.attach(scaler);
                        if (container.children.length == 1) {
                            container.add(control);
                        }
                        render();

                        RotNone();
                        $(".poly-animation-btn").removeClass("active");

                        document.getElementById("spinner").hidden = true;

                        var xhr = new XMLHttpRequest();
                        xhr.open("GET", obj.url, true);
                        xhr.send(null);
                        xhr.onreadystatechange = function() {
                            if (xhr.readyState == 4) {
                                if (xhr.status == 200) {
                                    actualObjSize = xhr.getResponseHeader(
                                        "Content-Length"
                                    );
                                    actualObjSize =
                                        actualObjSize == null
                                            ? -1
                                            : actualObjSize;
                                } else {
                                    console.log("ERROR");
                                    actualObjSize = -1;
                                }
                            }
                        };

                        $(".thumbnailText").removeClass("active");
                        $(".thumbnailImage").removeClass("active");
                        $(".thumbnailTrash").removeClass("active");
                        createThumbnails(
                            image.src,
                            actualModel,
                            actualModel.id
                        );
                    });
                });
            };
        }

        return image;
    }

    var raycaster = new THREE.Raycaster();
    var actualModelOut;

    function onMouseDown(event) {
        let canvasBounds = renderer.context.canvas.getBoundingClientRect();
        mouseInitX =
            ((event.clientX - canvasBounds.left) /
                (canvasBounds.right - canvasBounds.left)) *
                2 -
            1;
        mouseInitY =
            -(
                (event.clientY - canvasBounds.top) /
                (canvasBounds.bottom - canvasBounds.top)
            ) *
                2 +
            1;
    }

    window.addEventListener("mousedown", onMouseDown);

    $(".models-actif").on("click", ".thumbnailTrash", function() {
        $(".deleteModal").addClass("is-active");
        toDelete = $(this)
            .attr("class")
            .split(" ")[0];

        asset_delete_name.textContent = asset_name.textContent;
    });

    $(".add-scenar-btns .btn-form-primary").on("click", function() {
        $(".deleteModal").removeClass("is-active");
    });

    $(".add-scenar-btns .btn-form-secondary").on("click", function() {
        DeleteModel(toDelete);
        $(".deleteModal").removeClass("is-active");
    });

    function onMouseClick(event) {
        if (actualModel == null) return;
        saveModelInfo();
        let canvasBounds = renderer.context.canvas.getBoundingClientRect();
        var mousex =
            ((event.clientX - canvasBounds.left) /
                (canvasBounds.right - canvasBounds.left)) *
                2 -
            1;
        var mousey =
            -(
                (event.clientY - canvasBounds.top) /
                (canvasBounds.bottom - canvasBounds.top)
            ) *
                2 +
            1;

        if (mouseInitX != mousex || mouseInitY != mousey) return;

        var mouse = new THREE.Vector2(mousex, mousey);
        raycaster.setFromCamera(mouse, camera);

        var intersect = raycaster.intersectObjects(
            Array.from(objects.keys()),
            true
        )[0];

        if (intersect != null && intersect != undefined) {
            majModel(
                intersect.object.geometry.type == "PlaneGeometry"
                    ? intersect.object
                    : intersect.object.parent
            );
        }

        var thumbActif = $(".models-actif #thumbnailActif").filter(
            "[modelid='" + actualModel.id + "']"
        );
        var trashActif = $(".thumbnailTrash").filter(
            "[modelid='" + actualModel.id + "']"
        );

        $(".models-actif #thumbnailActif").removeClass("active");
        $(".thumbnailTrash").removeClass("active");
        thumbActif.addClass("active");
        trashActif.addClass("active");
    }

    $(".models-actif").on("click", "#thumbnailActif", function() {
        var actifActualModel = $(this).data("actualModel");
        var actifTrash = $(".thumbnailTrash").filter(
            "[modelid='" + actifActualModel.id + "']"
        );

        LoadModelOnClickImage(actifActualModel);

        $(".thumbnailText").removeClass("active");
        $(".thumbnailImage").removeClass("active");
        $(".thumbnailTrash").removeClass("active");
        $(this).addClass("active");
        actifTrash.addClass("active");
    });

    function LoadModelOnClickImage(actualModel) {
        saveModelInfo();
        majModel(actualModel);
    }

    function saveModelInfo() {
        var value = objects.get(actualModel);
        value.RotBeforeInf = rotationBeforeInfinite;
        value.previousRot = previousRot;
        value.speed = GetSpeed();
        value.rotX = rotX;
        value.rotY = rotY;
        value.rotZ = rotZ;
        value.transBeforeBoeing = translationBeforeBoeing;
        value.isBoeing = isBoeing;
    }

    function DeleteModel(id) {
        $("#thumbnailActif.active").remove();
        $(".thumbnailTrash.active").remove();

        asset_name.textContent = "Title";
        asset_author.textContent = "Author";

        if ($("#thumbnailActif").length == 2) {
            var thumbToActivate = $("#thumbnailActif:last-child");
            thumbToActivate.addClass("active");
        } else if ($("#thumbnailActif").length == 1) {
            var thumbToActivate = $("#thumbnailActif");
            thumbToActivate.addClass("active");
        }

        objects.delete(actualModel);
        container.remove(actualObject);
        actualModel = null;
        if (objects.size > 0) {
            majModel(Array.from(objects.keys())[0]);
            // console.log(Array.from(objects.keys())[0]);
        } else {
            container.remove(control);
        }

        if (id != "thumbnailTrash") {
            $.ajax({
                url:
                    urlPlatform +
                    "/api/waypoints/content/ar/delete/" +
                    waypointID +
                    "/" +
                    id,
                type: "GET",
                success: function(statut) {
                    $(".btn-model-delete").fadeOut(200);
                    $(".btn-model-edit-label").fadeOut(200);
                    $(".btn-model-free-label").fadeOut(200);
                    $(".btn-model-free-label.error").fadeOut(200);
                    $(".btn-model-free").removeClass("valided");
                    $(".btn-model-free-edit").removeClass("valided");
                    $(".btn-model-free").removeClass("unvalid");
                    $(".delete-poly-spin").fadeOut(200);
                },
                error: function(statut) {
                    console.log("500 delete poly", urlPlatform);
                }
            });
        }
    }

    function majModel(parent) {
        actualModel = parent;
        var value = objects.get(actualModel);

        console.log(value);

        actualUrl = value.url;
        asset_name.textContent = value.author;

        asset_author.textContent = value.name;

        actualObject = value.scaler;
        initialScale = value.initScale;
        initialRotation = value.initRot;
        initialPosition = value.initPos;
        rotationBeforeInfinite = value.RotBeforeInf;
        previousRot = value.previousRot;
        rotX = value.rotX;
        transBeforeBoeing = value.transBeforeBoeing;
        rotY = value.rotY;
        rotZ = value.rotZ;

        if (value.type != "IMG") box = value.box;

        isBoeing = value.isBoeing;
        translationBeforeBoeing = value.transBeforeBoeing;

        SetSpeed(value.speed);

        console.log(actualObject);

        control.attach(actualObject);
        render();
    }

    window.addEventListener("click", onMouseClick, false);

    /**
     * On results of the poly research
     * - Remove previous RESULTS
     * - Create thumbnails images for each result
     */
    function onResults(data) {
        while (results.childNodes.length) {
            results.removeChild(results.firstChild);
        }

        var assets = data.assets;

        if (assets) {
            for (var i = 0; i < assets.length; i++) {
                var asset = assets[i];
                var image = createImage(asset);
                results.appendChild(image);
            }
        } else {
            results.innerHTML = "<center>NO RESULTS</center>";
        }
    }

    search.addEventListener("submit", function(event) {
        event.preventDefault();

        searchPoly(query.value, onResults);
    });

    // Open model editor
    $(".buttonAr").on("click", function() {
        searchPoly(query.value, onResults);
    });

    function Recenter() {
        actualObject.position.multiply(new THREE.Vector3(0, 1, 0));
    }

    var toDelete;

    $(".editor-center").click(function() {
        Recenter();
    });

    $(".reinit-pos").click(function() {
        RstT();
        render();
    });

    $(".reinit-rot").click(function() {
        RstR();
        render();
    });

    $(".reinit-scale").click(function() {
        RstS();
        render();
    });

    $(".header-reset").click(function() {
        RstT();
        RstR();
        RstS();
        // $('.coordinates-btn').removeClass('active');
        $(".poly-animation-btn").removeClass("active");
        render();
    });

    var boiengStatus = true;
    $(".btn-boieng").click(function() {
        $(".btn-stop-rotate").removeClass("active");

        if (boiengStatus) {
            Boeing();
            $(this).addClass("active");
        } else {
            BoeingNone();
            $(this).removeClass("active");
        }

        boiengStatus ? (boiengStatus = false) : (boiengStatus = true);
    });

    $(".btn-rotate-x").click(function() {
        RotY();
        $(".btn-animation").removeClass("active");
        $(".btn-rotate-x").addClass("active");
    });

    $(".btn-rotate-y").click(function() {
        RotX();
        $(".btn-animation").removeClass("active");
        $(".btn-rotate-y").addClass("active");
    });

    $(".btn-rotate").click(function() {
        RotZ();
        $(".btn-animation").removeClass("active");
        $(".btn-rotate").addClass("active");
    });

    $(".btn-stop-rotate").click(function() {
        if (!boiengStatus) {
            BoeingNone();
            boiengStatus = true;
        }
        RotNone();
        $(".btn-animation").removeClass("active");
        $(".btn-animation-boeing").removeClass("active");
        $(".btn-stop-rotate").addClass("active");
    });

    function loadAllAssets(json) {
        for (var i = 0; i < $(".poly-json").length; i++) {
            jsonModel = $(".poly-json")
                .eq(i)
                .text();
            idBack = $(".poly-json")
                .eq(i)
                .attr("id");
            infos = JSON.parse(jsonModel);
            infosType = $(".arMediaType")
                .eq(i)
                .html();
            infosFile = $(".arMediaFile")
                .eq(i)
                .html();

            console.log(infos);

            if (
                infosFile.includes(".jpg") ||
                infosFile.includes(".jpeg") ||
                infosFile.includes(".png") ||
                infosFile.includes(".gif")
            ) {
                isImage = true;
            }

            if (infos.modelInfo) {
                console.log("Load old poly");
                isOldPoly = true;
                loadAsset(infos, "POLY", null, null, true);
            } else if (infos.fromEditor && isImage) {
                LoadImage(infosFile, "Image-" + idBack, null);
                console.log("Load old Image");
            } else {
                console.log("Load new json");
                loadAsset(infos, infosType, idBack, infosFile);
            }

            $(".aug-choice-btn").removeClass("active");
            $(".aug-choice").removeClass("active");

            if (infos.box && infos.box.boxActivated) {
                selectedChoice = "box";
                isBoxColor = true;
                isBoxAnimation = true;
                $(".aug-choice-box").addClass("active");
                $(".box-choice-btn").addClass("active");
                animationSelectedDiv = $(".animation-box").filter(
                    "[animation=" + infos.box.boxAnimation + "]"
                );
                animationSelectedDiv.addClass("active");

                $(".color-preview").css("backgroundColor", infos.box.boxColor);
                $(".hexcolor").val(infos.box.boxColor);

                animationSelected = infos.box.boxAnimation;
                newHex = infos.box.boxColor;

                boxInit(infos.box.boxColor, infos.box.boxAnimation);

                canOpenBoxEditor();
            } else {
                selectedChoice = "notBox";
                $(".aug-choice-float").addClass("active");
                $(".float-choice-btn").addClass("active");
            }
        }
    }

    function canOpenBoxEditor() {
        $(".btn-aug-box").addClass("active");
        $(".btn-aug-box-indic").css("display", "none");
    }

    function saveEdit(elementSelected, boxElement, saveBlock, saveButton) {
        console.log("saveEdit");
        if (polyJson.length > 0 && infos.box) {
            if (boxElement && elementSelected != boxElement) {
                $(saveBlock).fadeIn();
                $(saveButton).addClass("active");
            } else {
                $(saveBlock).fadeOut(100);
                $(saveButton).removeClass("active");
            }
        }
    }

    function boxInit(color, anim) {
        boxActivated = true;
        boxColor = color;
        boxAnimation = anim;

        var matTransparent = new THREE.MeshBasicMaterial({
            transparent: true,
            opacity: 0.4,
            color: color
        });
        var mat = new THREE.MeshBasicMaterial({ color: color });
        mat.side = THREE.DoubleSide;
        //matTransparent.side = THREE.DoubleSide;

        plane.material = mat;

        planeleft = new THREE.Mesh(geometry, matTransparent);
        //planeleft.rotateX(1.5708);
        //plane.rotateZ(1.5708*2);
        planeleft.rotateY(1.5708 * 3);
        planeleft.position.y += 2.5;
        planeleft.position.x += 2.5;

        scene.add(planeleft);

        planeRight = new THREE.Mesh(geometry, matTransparent);
        //planeRight.rotateX(1.5708);
        //plane.rotateZ(1.5708*2);
        planeRight.rotateY(1.5708);
        planeRight.position.y += 2.5;
        planeRight.position.x -= 2.5;

        scene.add(planeRight);

        planeBack = new THREE.Mesh(geometry, matTransparent);
        //planeBack.rotateX(1.5708);
        //plane.rotateZ(1.5708*2);
        planeBack.rotateY(1.5708 * 2);
        planeBack.position.y += 2.5;
        planeBack.position.z += 2.5;

        scene.add(planeBack);

        planeTop = new THREE.Mesh(geometry, matTransparent);
        planeTop.rotateX(1.5708 * 3);
        planeTop.position.y += 5;

        // scene.add(planeTop);

        var textureArrow, matArrow;
        textureArrow = new THREE.TextureLoader().load(
            "../../modelEditor/marker.jpg",
            function() {
                render();
            }
        );
        textureArrow.wrapS = THREE.RepeatWrapping;
        textureArrow.wrapT = THREE.RepeatWrapping;

        textureArrow.repeat.set(1, 1);

        matArrow = new THREE.MeshBasicMaterial({
            map: textureArrow,
            alphaTest: 0.5,
            transparent: true
        });

        geometryArrow = new THREE.PlaneGeometry(4.4, 4.4);

        planeArrow = new THREE.Mesh(geometryArrow, matArrow);
        planeArrow.material.side = THREE.DoubleSide;
        planeArrow.rotateX(6.2811);
        planeArrow.rotateZ(6.28);
        planeArrow.rotateY(1.5708 * 2);
        planeArrow.position.z += 3;

        planeArrow.position.y += 2.5;

        scene.add(planeArrow);

        var textureUser, matUser;

        textureUser = new THREE.TextureLoader().load(
            "../../modelEditor/eye.png",
            function() {
                render();
            }
        );
        textureUser.wrapS = THREE.RepeatWrapping;
        textureUser.wrapT = THREE.RepeatWrapping;

        textureUser.repeat.set(1, 1);

        matUser = new THREE.MeshBasicMaterial({
            map: textureUser,
            alphaTest: 0.5,
            transparent: true
        });

        geometryUser = new THREE.PlaneGeometry(1, 1);

        planeUser = new THREE.Mesh(geometryUser, matUser);
        planeUser.material.side = THREE.DoubleSide;
        planeUser.rotateX(1.5708);
        planeUser.rotateZ(6.3);
        planeUser.position.z -= 4;

        planeUser.position.y += 1;

        scene.add(planeUser);
    }

    function loadAsset(info, type, idBack, filePath, isOld = false) {
        console.log("LOAD ASSET !");
        console.log(isOld);

        var info,
            id,
            scale,
            position,
            rotation,
            infiniteRotAxis,
            infiniteRotSpeed,
            mInverse,
            boeing,
            type;

        var typeAr;
        if (type == " POLY " || type == "POLY") {
            typeAr = "POLY";
        }

        // if(typeAr != "POLY") {
        //   isLoaded = true;
        //   loadUserModel(info, type, filePath, idBack, isLoaded);
        //   return;
        // }

        if (isOld) {
            id = info.modelInfo.id;
            scale = new THREE.Vector3(
                info.modelInfo.scale.x,
                info.modelInfo.scale.y,
                info.modelInfo.scale.z
            );
            position = new THREE.Vector3(
                info.modelInfo.position.x,
                info.modelInfo.position.z,
                info.modelInfo.position.y
            );
            rotation = new THREE.Quaternion(
                info.modelInfo.rotation.x,
                -info.modelInfo.rotation.y,
                -info.modelInfo.rotation.z,
                info.modelInfo.rotation.w
            );
            infiniteRotAxis = info.modelInfo.infiniteRotation.axe;
            infiniteRotSpeed = info.modelInfo.infiniteRotation.speed;
            mInverse = new THREE.Matrix4().getInverse(plane.matrixWorld);
            boeing = 0;
        } else {
            // type = info.type;
            if (typeAr != "POLY") {
                isLoaded = true;
                loadUserModel(info, type, filePath, idBack, isLoaded);
                return;
            }

            id = info.id;
            scale = new THREE.Vector3(info.scale.x, info.scale.y, info.scale.z);
            position = new THREE.Vector3(
                info.position.x,
                info.position.z,
                info.position.y
            );
            rotation = new THREE.Quaternion(
                info.rotation.x,
                -info.rotation.y,
                -info.rotation.z,
                info.rotation.w
            );
            infiniteRotAxis = info.infiniteRotation.axe;
            infiniteRotSpeed = info.infiniteRotation.speed;
            mInverse = new THREE.Matrix4().getInverse(plane.matrixWorld);
            boeing = info.isBoeing;
        }

        var url = `https://poly.googleapis.com/v1/assets/${id}/?key=${API_KEY}`;

        var request = new XMLHttpRequest();
        request.open("GET", url, true);
        request.addEventListener("load", function(event) {
            var asset = JSON.parse(event.target.response);

            asset_name.textContent = asset.displayName;
            asset_author.textContent = asset.authorName;

            asset_delete_name.textContent = asset.displayName;

            var format = asset.formats.find(format => {
                return format.formatType === "OBJ";
            });

            if (format !== undefined) {
                var obj = format.root;
                var mtl = format.resources.find(resource => {
                    return resource.url.endsWith("mtl");
                });

                var path = obj.url.slice(0, obj.url.indexOf(obj.relativePath));
                actualUrl = path;

                var loader = new THREE.MTLLoader();
                loader.setCrossOrigin(true);
                loader.setMaterialOptions({ ignoreZeroRGBs: true });
                loader.setTexturePath(path);
                loader.load(mtl.url, function(materials) {
                    var loader = new THREE.OBJLoader();
                    loader.setMaterials(materials);
                    loader.load(obj.url, function(object) {
                        // var id = info.id;
                        // var scale = new THREE.Vector3(info.scale.x, info.scale.y, info.scale.z);
                        // var position = new THREE.Vector3(info.position.x, info.position.z, info.position.y);
                        // var rotation = new THREE.Quaternion(info.rotation.x, -info.rotation.y, -info.rotation.z, info.rotation.w);
                        // var infiniteRotAxis = info.infiniteRotation.axe;
                        // var infiniteRotSpeed = info.infiniteRotation.speed;
                        // var mInverse = new THREE.Matrix4().getInverse(plane.matrixWorld);

                        // var boeing = info.isBoeing;

                        box = new THREE.Box3();
                        box.setFromObject(object);

                        // re-center
                        var center = box.getCenter();
                        // center.y = box.min.y;
                        object.position.sub(center);

                        // scale
                        var scaler = new THREE.Group();
                        scaler.add(object);
                        scaler.scale.setScalar(8 / box.getSize().length());

                        // Set actual Model and transforming object variables
                        actualModel = object;
                        actualObject = scaler;

                        if (!isOld) {
                            if (typeAr == "POLY") {
                                createThumbnailsEdit(
                                    info.thumbUrl,
                                    actualModel,
                                    actualModel.id,
                                    idBack
                                );
                            } else {
                                createThumbnailText(
                                    info.author,
                                    actualModel,
                                    actualModel.id
                                );
                            }
                        }

                        // console.log(position)

                        position.applyMatrix4(plane.matrixWorld);
                        // console.log(position)
                        position.multiplyScalar(5);

                        console.log(position);

                        // position.multiplyScalar(6 / box.getSize().length());
                        // position.add(offset);
                        actualObject.position.x = position.x;
                        actualObject.position.y = position.y;
                        actualObject.position.z = position.z;

                        console.log(position);

                        scale.multiplyScalar(8 / box.getSize().length());
                        actualObject.scale.x = scale.x;
                        actualObject.scale.y = scale.y;
                        actualObject.scale.z = scale.z;

                        actualObject.quaternion.x = rotation.x;
                        actualObject.quaternion.y = rotation.y;
                        actualObject.quaternion.z = rotation.z;
                        actualObject.quaternion.w = rotation.w;

                        container.add(scaler);
                        // Initial informations on object for Reset
                        initialPosition = new THREE.Vector3().copy(
                            scaler.position
                        );
                        initialScale = new THREE.Vector3().copy(scaler.scale);
                        initialRotation = new THREE.Quaternion().copy(
                            scaler.quaternion
                        );

                        if (!isSearching) {
                            switch (infiniteRotAxis) {
                                case "X":
                                    RotX();
                                    $(".btn-rotate-x").addClass("active");
                                    break;
                                case "Y":
                                    RotY();
                                    $(".btn-rotate-y").addClass("active");
                                    break;
                                case "Z":
                                    RotZ();
                                    $("btn-rotate").addClass("active");
                                    break;
                            }
                        }

                        $("#speed").val(infiniteRotSpeed);

                        document.getElementById(
                            "speed"
                        ).value = infiniteRotSpeed;

                        if (boeing == 1) {
                            Boeing();
                        } else {
                            translationBeforeBoeing = null;
                            BoeingNone();
                        }

                        var value = {
                            extension: type,
                            id: id,
                            thumbUrl: info.thumbUrl,
                            url: actualUrl,
                            author: info.name,
                            name: info.author,
                            scaler: scaler,
                            initPos: initialPosition,
                            initScale: initialScale,
                            initRot: initialRotation,
                            RotBeforeInf: rotationBeforeInfinite,
                            previousRot: previousRot,
                            rotX: rotX,
                            rotY: rotY,
                            rotZ: rotZ,
                            speed: infiniteRotSpeed,
                            isBoeing: boeing,
                            transBeforeBoeing: translationBeforeBoeing,
                            isOld: true,
                            idBack: idBack,
                            box: box
                        };
                        objects.set(object, value);

                        // Attach Gizmo transform control to object
                        control.attach(scaler);
                        if (container.children.length == 1) {
                            container.add(control);
                        }
                        render();

                        // Model is loaded => Hide spinner
                        document.getElementById("spinner").hidden = true;

                        var xhr = new XMLHttpRequest();
                        xhr.open("HEAD", obj.url, true);
                        xhr.withCredentials = true;
                        xhr.send(null);
                        xhr.onreadystatechange = function() {
                            if (xhr.readyState == 4) {
                                if (xhr.status == 200) {
                                    actualObjSize = xhr.getResponseHeader(
                                        "Content-Length"
                                    );
                                    actualObjSize =
                                        actualObjSize == null
                                            ? -1
                                            : actualObjSize;
                                    // console.log('Size: ' + actualObjSize + " octets");
                                } else {
                                    console.log("ERROR");
                                    actualObjSize = -1;
                                }
                            }
                        };
                    });
                });
            }
        });

        request.send(null);
    }

    /**
     * Loads a user model from JSON
     *
     */
    function loadUserModel(info, type, filePath, idBack, isLoaded) {
        while (container.children.length) {
            container.remove(container.children[0]);
        }

        var id = filePath;
        if (info.mtl) {
            var mtl = info.mtl;
        }
        if (info.texturePath) {
            var texturePath = info.texturePath;
        }

        type = type.replace(/\s/g, "");
        if (type == "gif" || type == "png" || type == "jpg" || type == "jpeg") {
            type = "image";
        }

        var webString = filePath;
        var webFile = "https://" + webString.replace(/ /g, ""); /* DEV/PROD */

        /****** DEBUG LOCAL ******/
        // var webFile = "https://dev.hackeo.pro/cdn/9aaa0b523023790b2cf1df9dc629ba14abd0edd55b3e26b2411a2/3207f215799df79eb2ba393821fa204bd8e436f7.3ds"; /* LOCAL 3DS */
        // var webFile = "http://www.zoonewengland.org/media/813822/redpanda_gallery10.jpg"; /* LOCAL Image  */
        // var webFile = "https://dev.hackeo.pro/cdn/9aaa0b523023790b2cf1df9dc629ba14abd0edd55b3e26b2411a2/29bee50d482d4b4000a4064213841953d9c3d287.obj"; /* LOCAL OBJ */

        asset_name.textContent = info.name;
        asset_author.textContent = info.author;

        asset_delete_name.textContent = info.name;

        console.log(type);
        console.log(webFile);
        console.log(info);

        switch (type) {
            case "fbx":
                LoadFBX(webFile, info.name, isLoaded, idBack, () => {
                    callBackLoadingModel(info, type);
                });
                break;
            case "obj":
                if (info.mtl) {
                    LoadOBJ(
                        webFile,
                        mtl,
                        "",
                        info.name,
                        isLoaded,
                        idBack,
                        () => {
                            callBackLoadingModel(info, type);
                        }
                    );
                }
                if (info.texturePath) {
                    LoadOBJ(
                        webFile,
                        "",
                        texturePath,
                        info.name,
                        isLoaded,
                        idBack,
                        () => {
                            callBackLoadingModel(info, type);
                        }
                    );
                }
                break;
            case "dae":
                LoadCollada(webFile, info.name, isLoaded, idBack, () => {
                    callBackLoadingModel(info, type);
                });
                break;
            case "3ds":
                Load3DS(
                    webFile,
                    texturePath,
                    info.name,
                    isLoaded,
                    idBack,
                    () => {
                        callBackLoadingModel(info, type);
                    }
                );
                break;
            case "image":
                setTimeout(function() {
                    LoadImage(webFile, info.name, isLoaded, idBack, () => {
                        callBackLoadingModel(info, type);
                    });
                }, 1500);
                break;
            default:
                break;
        }
    }

    function callBackLoadingModel(info, type) {
        console.log("LoadUserModel() >> actual Object: " + actualObject);

        var scale = new THREE.Vector3(info.scale.x, info.scale.y, info.scale.z);
        var position = new THREE.Vector3(
            info.position.x,
            info.position.z,
            info.position.y
        );
        var rotation = new THREE.Quaternion(
            info.rotation.x,
            -info.rotation.y,
            -info.rotation.z,
            info.rotation.w
        );
        var infiniteRotAxis = info.infiniteRotation.axe;
        var infiniteRotSpeed = info.infiniteRotation.speed;
        var mInverse = new THREE.Matrix4().getInverse(plane.matrixWorld);

        var boeing = info.isBoeing;

        position.applyMatrix4(plane.matrixWorld);
        position.multiplyScalar(5);

        actualObject.position.x = position.x;
        actualObject.position.y = position.y;
        actualObject.position.z = position.z;

        // if(type != "jpeg") {
        //   asset_name.textContent = info.author;
        //   asset_author.textContent = info.name;
        // } else {
        //   asset_name.textContent = info.name;
        //   asset_author.textContent = info.author;
        // }

        if (type != "image") {
            scale.multiplyScalar(8 / box.getSize().length());
        }

        actualObject.scale.x = scale.x;
        actualObject.scale.y = scale.y;
        actualObject.scale.z = scale.z;

        actualObject.quaternion.x = rotation.x;
        actualObject.quaternion.y = rotation.y;
        actualObject.quaternion.z = rotation.z;
        actualObject.quaternion.w = rotation.w;

        initialPosition = new THREE.Vector3().copy(actualObject.position);
        initialScale = new THREE.Vector3().copy(actualObject.scale);
        initialRotation = new THREE.Quaternion().copy(actualObject.quaternion);

        switch (infiniteRotAxis) {
            case "X":
                RotX();
                // document.getElementById("rotX").checked = true;
                break;
            case "Y":
                RotY();
                // document.getElementById("rotY").checked = true;
                break;
            case "Z":
                RotZ();
                // document.getElementById("rotZ").checked = true;
                break;
        }

        document.getElementById("speed").value = infiniteRotSpeed;

        if (boeing == 1) {
            Boeing();
        } else {
            translationBeforeBoeing = null;
            BoeingNone();
        }

        render();
    }

    function switchCamera() {
        var markerStatus = $("#Sol").text();

        if (
            camStatus == "SouthE" ||
            camStatus == "SouthO" ||
            camStatus == "NorthE" ||
            camStatus == "NorthO"
        ) {
            //camera.position.set(up.x, up.y, up.z);
            targetPos = new THREE.Vector3().copy(up);
            //targetRot = new THREE.Quaternion().copy(upRot);
            isMoving = true;
            previousCamStatus = camStatus;
            camStatus = "UP";
        } else {
            var toCopy;
            switch (previousCamStatus) {
                case "SouthE":
                    toCopy = southE;
                    break;
                case "SouthO":
                    toCopy = southO;
                    break;
                case "NorthE":
                    toCopy = northE;
                    break;
                default:
                    toCopy = northO;
                    break;
            }
            targetPos = new THREE.Vector3().copy(toCopy);
            //targetRot = new THREE.Quaternion().copy(frontRot);
            isMoving = true;
            camStatus = previousCamStatus;
        }

        camera.lookAt(0, 1.5, 0);
        render();
    }

    /**
     * Switch camera view to the foolowing left
     */
    function switchCameraLeft() {
        if (camStatus == "UP") return;
        var toCopy;
        switch (camStatus) {
            case "SouthE":
                toCopy = southO;
                camStatus = "SouthO";
                break;
            case "SouthO":
                toCopy = northO;
                camStatus = "NorthO";
                break;
            case "NorthE":
                toCopy = southE;
                camStatus = "SouthE";
                break;
            default:
                toCopy = northE;
                camStatus = "NorthE";
                break;
        }
        isMoving = true;
        targetPos = new THREE.Vector3().copy(toCopy);

        camera.lookAt(0, 1.5, 0);
        render();
    }

    /**
     * Switch camera view to the foolowing right
     */
    function switchCameraRight() {
        if (camStatus == "UP") return;
        var toCopy;
        switch (camStatus) {
            case "SouthE":
                toCopy = northE;
                camStatus = "NorthE";
                break;
            case "SouthO":
                toCopy = southE;
                camStatus = "SouthE";
                break;
            case "NorthE":
                toCopy = northO;
                camStatus = "NorthO";
                break;
            default:
                toCopy = southO;
                camStatus = "SouthO";
                break;
        }
        isMoving = true;
        targetPos = new THREE.Vector3().copy(toCopy);

        camera.lookAt(0, 1.5, 0);
        render();
    }

    function init() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize(window.innerWidth, window.innerHeight);

        window.addEventListener("keydown", function(event) {
            if (!isSearching) {
                switch (event.keyCode) {
                    case 81: // Q, change transform space from loacl/world
                        control.setSpace(
                            control.space === "local" ? "world" : "local"
                        );
                        break;

                    case 17: // Ctrl, Snapping
                        control.setTranslationSnap(100);
                        control.setRotationSnap(THREE.Math.degToRad(15));
                        break;

                    case 87: // W, Translate mode
                        control.setSpace("world");
                        control.setMode("translate");
                        $(".btn-edition").removeClass("active");
                        $(".editor-pos").addClass("active");
                        break;

                    case 69: // E, Rotate Mode
                        control.setSpace("world");
                        control.setMode("rotate");
                        $(".btn-edition").removeClass("active");
                        $(".editor-rot").addClass("active");
                        break;

                    case 82: // R, Scale Mode
                        control.setSpace("local");
                        control.setMode("scale");
                        $(".btn-edition").removeClass("active");
                        $(".editor-dimension").addClass("active");
                        break;

                    // case 67: // C, switch camera point of view
                    // switchCamera();
                    // if(camStatus == "UP") {
                    //   $('.switch-cam .switch input').removeAttr('checked', 'checked');
                    //
                    // } else {
                    //   $('.switch-cam .switch input').attr('checked', 'checked');
                    // }

                    // console.log(camStatus);
                    // break;

                    case 88: // X, switch camera to right
                        switchCameraLeft();
                        break;

                    case 86: // V, switch camera to left
                        switchCameraRight();
                        break;

                    // GIzmos size (Unuzed)
                    case 187:
                    case 107: // +, =, num+
                        control.setSize(control.size + 0.1);
                        break;

                    case 189:
                    case 109: // -, _, num-
                        control.setSize(Math.max(control.size - 0.1, 0.1));
                        break;
                }
            }
        });

        render();

        // window.addEventListener( 'resize', onWindowResize, true );
        $(".coordinates-btn").click(function() {
            $(".coordinates-btn").removeClass("active");
            $(this).addClass("active");
        });

        $(".poly-animation-btn").click(function() {
            $(".poly-animation-btn").removeClass("active");
            $(this).addClass("active");
        });

        $(".editor-pos").click(function() {
            control.setSpace("world");
            control.setMode("translate");
            $(".btn-edition").removeClass("active");
            $(".editor-pos").addClass("active");
        });

        $(".editor-rot").click(function() {
            control.setSpace("world");
            control.setMode("rotate");
            $(".btn-edition").removeClass("active");
            $(".editor-rot").addClass("active");
        });

        $(".editor-dimension").click(function() {
            control.setSpace("local");
            control.setMode("scale");
            $(".btn-edition").removeClass("active");
            $(".editor-dimension").addClass("active");
        });

        $(".camera-switch").click(function() {
            switchCamera();
        });

        $("#marker-position-input").click(function() {
            switchCamMarker();
        });

        $("#world-to-left").click(function() {
            switchCameraLeft();
        });

        $("#world-to-right").click(function() {
            switchCameraRight();
        });
    }

    function switchCamMarker() {
        var markerStatus = $("#marker-position").text();

        if (
            camStatus == "SouthE" ||
            camStatus == "SouthO" ||
            camStatus == "NorthE" ||
            (camStatus == "NorthO" && markerStatus == "Mur") ||
            (selectedChoice == "notBox" && markerStatus == "Mur")
        ) {
            targetPos = new THREE.Vector3().copy(up);
            isMoving = true;
            previousCamStatus = camStatus;
            camStatus = "UP";

            $(".arrow-changing").css("display", "none");
            $("#marker-position").text("Mur");
        } else if (markerStatus == "Sol" || markerStatus == "Mur") {
            var toCopy;
            switch (previousCamStatus) {
                case "SouthE":
                    toCopy = southE;
                    break;
                case "SouthO":
                    toCopy = southO;
                    break;
                case "NorthE":
                    toCopy = northE;
                    break;
                default:
                    toCopy = northO;
                    break;
            }
            targetPos = new THREE.Vector3().copy(toCopy);
            isMoving = true;
            camStatus = previousCamStatus;

            $(".arrow-changing").css("display", "block");
            $("#marker-position").text("Sol");
        }

        camera.lookAt(0, 1.5, 0);
        render();
    }

    $("#save-model").click(function() {
        if (container.children.length == 0) {
            $(".no-3d-model").addClass("actif");

            setTimeout(function() {
                $(".no-3d-model").removeClass("actif");
            }, 5000);
        } else {
            GetCompleteJSON();
            $(".savePoly").fadeIn();
            $(".spinner-bg").fadeIn();
        }
    });

    function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize(window.innerWidth, window.innerHeight);

        render();
    }

    function render() {
        control.update();

        renderer.render(scene, camera);
    }

    function GetCoordinates() {
        var mInverse = new THREE.Matrix4().getInverse(plane.matrixWorld);
        var position = new THREE.Vector3().copy(actualObject.position);
        var scale = new THREE.Vector3().copy(actualObject.scale);
        // var rotation = new THREE.Vector3().copy(actualObject.rotation);
        var rotation = new THREE.Quaternion().copy(actualObject.quaternion);
        if (
            document.querySelector("input[name=infiniteRot]:checked").value !=
            "None"
        )
            rotation = new THREE.Vector3().copy(rotationBeforeInfinite);
        //Peut-Ãªtre voir pour utiliser "quaternion"

        // rotation.multiplyScalar(57.2958); // Radian -> Degree
        // rotation.applyMatrix4(mInverse);
        position.sub(offset);
        position.multiplyScalar(box.getSize().length() / 6); //Rescale position with marker size
        position.multiplyScalar(1 / 5);

        position.applyMatrix4(mInverse);

        scale.multiplyScalar(box.getSize().length() / 6);

        var splitted = actualUrl.split("/");
        var id = splitted[splitted.length - 3];

        document.getElementById("objInfos").innerHTML =
            id +
            "<br/>" +
            actualObjSize +
            " octets <br/>" +
            "T : X: " +
            position.x.toFixed(2) +
            ", Y: " +
            position.z.toFixed(2) +
            ", Z: " +
            position.y.toFixed(2) +
            "<br/>" +
            "R : X: " +
            rotation.x.toFixed(2) +
            ", Y: " +
            -rotation.y.toFixed(2) +
            ", Z: " +
            -rotation.z.toFixed(2) +
            ", W: " +
            rotation.w.toFixed(2) +
            "</br>" +
            "S : X: " +
            scale.x.toFixed(2) +
            ", Y: " +
            scale.y.toFixed(2) +
            ", Z: " +
            scale.z.toFixed(2) +
            "<br/>";
    }

    /**
     * Returns the complete JSON string with all models
     */
    function GetCompleteJSON() {
        var jsonStr = "{\n";
        jsonStr += '"modelNumber": ' + objects.size + ",\n";
        var cpt = 0;

        for (var [key, value] of objects) {
            jsonStr += '"model' + cpt + '":';
            majModel(key);
            jsonStr += JSONString() + ",\n";
            cpt++;
        }

        jsonStr = jsonStr.substring(0, jsonStr.length - 2);
        jsonStr += "\n}";

        copyJSONToClipboard(jsonStr);
        return jsonStr;
    }

    /**
     * Returns the JSON string for one model
     */
    function JSONString() {
        // Coordinates change matrix
        var mInverse = new THREE.Matrix4().getInverse(plane.matrixWorld);
        var position = new THREE.Vector3().copy(actualObject.position);
        var scale = new THREE.Vector3().copy(actualObject.scale);
        var rotation = new THREE.Quaternion().copy(actualObject.quaternion);
        // if(document.querySelector('input[name=infiniteRot]:checked').value != "None")
        // rotation = new THREE.Quaternion().copy(rotationBeforeInfinite);
        //Peut-Ãªtre voir pour utiliser "quaternion"

        //position.add(offset);
        //position.multiplyScalar(box.getSize().length() / 6); //Rescale position with marker size

        console.log(position);
        position.multiplyScalar(1 / 5);
        position.applyMatrix4(mInverse);

        var value = objects.get(actualModel);

        if (
            value.extension == "gif" ||
            value.extension == "png" ||
            value.extension == "jpg"
        ) {
            typeFile = "image";
        } else {
            typeFile = "model3d";
        }

        if (typeFile == "model3d") {
            scale.multiplyScalar(box.getSize().length() / 8);
        }

        // Retrieve model ID
        var splitted;
        var id;
        var thumbUrl = null;
        var file;

        var mursol = document.getElementById("marker-position").innerText;
        var axe = GetRotationAxis();
        var speed = GetSpeed();

        var mtlPath = null;

        var jsonInfos = [];

        if (value.extension == "obj") mtlPath = value.mtl;

        console.log(value);

        if (selectedChoice == "notBox") {
            boxActivated = null;
            boxColor = null;
            boxAnimation = null;
        }

        if (value.extension == " POLY " || value.extension == "POLY") {
            splitted = actualUrl.split("/");
            id = splitted[splitted.length - 3];
            thumbUrl = value.thumbUrl;

            if (value.isOld) {
                jsonInfos = infosToJSON(
                    "POLY",
                    value.id,
                    position.x.toFixed(2),
                    position.z.toFixed(2),
                    position.y.toFixed(2),
                    rotation.x.toFixed(7),
                    -rotation.y.toFixed(7),
                    -rotation.z.toFixed(7),
                    rotation.w.toFixed(7),
                    scale.x.toFixed(2),
                    scale.y.toFixed(2),
                    scale.z.toFixed(2),
                    value.isBoeing,
                    mursol,
                    axe,
                    speed,
                    actualObjSize ? actualObjSize : -1,
                    value.name,
                    value.author,
                    mtlPath,
                    value.texturePath,
                    value.thumbUrl,
                    null,
                    boxActivated,
                    boxColor,
                    boxAnimation
                );
            } else {
                jsonInfos = infosToJSON(
                    "POLY",
                    id,
                    position.x.toFixed(2),
                    position.z.toFixed(2),
                    position.y.toFixed(2),
                    rotation.x.toFixed(7),
                    -rotation.y.toFixed(7),
                    -rotation.z.toFixed(7),
                    rotation.w.toFixed(7),
                    scale.x.toFixed(2),
                    scale.y.toFixed(2),
                    scale.z.toFixed(2),
                    value.isBoeing,
                    mursol,
                    axe,
                    speed,
                    actualObjSize ? actualObjSize : -1,
                    value.name,
                    value.author,
                    mtlPath,
                    value.texturePath,
                    value.thumbUrl,
                    null,
                    boxActivated,
                    boxColor,
                    boxAnimation
                );
            }
        } else {
            file = '"' + actualUrl + '"';
            // scale.divide(initialScale);

            var jsonInfos = infosToJSON(
                value.extension,
                value.id,
                position.x.toFixed(2),
                position.z.toFixed(2),
                position.y.toFixed(2),
                rotation.x.toFixed(7),
                -rotation.y.toFixed(7),
                -rotation.z.toFixed(7),
                rotation.w.toFixed(7),
                scale.x.toFixed(2),
                scale.y.toFixed(2),
                scale.z.toFixed(2),
                value.isBoeing,
                mursol,
                axe,
                speed,
                actualObjSize ? actualObjSize : -1,
                value.name,
                value.author,
                mtlPath,
                value.texturePath,
                value.thumbUrl,
                file,
                boxActivated,
                boxColor,
                boxAnimation
            );
        }

        // return jsonInfos;
        console.log("JSON INFO " + jsonInfos);
        console.log(waypointID);

        if (value.idBack) {
            $.ajax({
                url:
                    urlPlatform +
                    "/api/waypoints/content/ar/edit/" +
                    value.idBack,
                type: "POST",
                data: jsonInfos,
                success: function(statut) {
                    $("#configPoly").modal("hide");
                    $(".savePoly").fadeOut();
                    $(".spinner-bg").fadeOut();

                    if (boxActivated) {
                        $(".aug-choice-box .btn-model-free").addClass(
                            "valided"
                        );
                        $(".aug-choice-box .btn-model-free-edit").addClass(
                            "valided"
                        );
                    } else {
                        $(".aug-choice-float .btn-model-free").addClass(
                            "valided"
                        );
                        $(".aug-choice-float .btn-model-free-edit").addClass(
                            "valided"
                        );
                    }

                    $(".anim-open-block #save-edit-spinner").fadeOut();
                    $(".box-color-block #save-edit-spinner").fadeOut();

                    $(".btn-model-edit-label.show")
                        .css({
                            display: "inline-block"
                        })
                        .animate({ opacity: 1 }, 600);

                    if (boxActivated) {
                        $(".aug-choice-box .btn-model-free-label.success")
                            .css({
                                display: "inline-block"
                            })
                            .animate({ opacity: 1 }, 600);
                    } else {
                        $(".aug-choice-float .btn-model-free-label.success")
                            .css({
                                display: "inline-block"
                            })
                            .animate({ opacity: 1 }, 600);
                    }

                    $(".btn-model-free-label.error").css({
                        display: "none",
                        opacity: 0
                    });
                    $(".btn-model-delete")
                        .css({
                            display: "inline-block"
                        })
                        .animate({ opacity: 1 }, 600);
                },
                error: function(statut) {
                    console.log("500 poly", urlPlatform, statut);
                    $("#configPoly").modal("hide");
                    $(".savePoly").fadeOut();
                    $(".spinner-bg").fadeOut();
                    $(".btn-model-free-label").addClass("unvalid");
                    $(".btn-model-free").addClass("unvalid");
                    $(".btn-model-free-label.error")
                        .css({
                            opacity: 0,
                            display: "inline-block"
                        })
                        .animate({ opacity: 1 }, 600);
                    // $('.btn-model-edit-label.show').css({
                    //   display: 'none'
                    // }).animate({opacity:1},600);
                    $(".btn-model-free-label.success").css({
                        display: "none",
                        opacity: 0
                    });
                    $(".btn-model-delete")
                        .css({
                            display: "inline-block"
                        })
                        .animate({ opacity: 1 }, 600);
                }
            });
        } else {
            $.ajax({
                url:
                    urlPlatform + "/api/waypoints/content/ar/add/" + waypointID,
                type: "PUT",
                data: jsonInfos,
                success: function(statut) {
                    $(".put-3d-success").addClass("actif");

                    setTimeout(function() {
                        $(".put-3d-success").removeClass("actif");
                    }, 5000);
                    $("#configPoly").modal("hide");
                    $(".savePoly").fadeOut();
                    $(".spinner-bg").fadeOut();
                    if (boxActivated) {
                        $(".aug-choice-box .btn-model-free").addClass(
                            "valided"
                        );
                        $(".aug-choice-box .btn-model-free-edit").addClass(
                            "valided"
                        );
                    } else {
                        $(".aug-choice-float .btn-model-free").addClass(
                            "valided"
                        );
                        $(".aug-choice-float .btn-model-free-edit").addClass(
                            "valided"
                        );
                    }
                    $(".anim-open-block #save-edit-spinner").fadeOut();
                    $(".box-color-block #save-edit-spinner").fadeOut();

                    $(".btn-model-edit-label.show")
                        .css({
                            display: "inline-block"
                        })
                        .animate({ opacity: 1 }, 600);

                    if (boxActivated) {
                        $(".aug-choice-box .btn-model-free-label.success")
                            .css({
                                display: "inline-block"
                            })
                            .animate({ opacity: 1 }, 600);
                    } else {
                        $(".aug-choice-float .btn-model-free-label.success")
                            .css({
                                display: "inline-block"
                            })
                            .animate({ opacity: 1 }, 600);
                    }

                    $(".btn-model-free-label.error").css({
                        display: "none",
                        opacity: 0
                    });
                    $(".btn-model-delete")
                        .css({
                            display: "inline-block"
                        })
                        .animate({ opacity: 1 }, 600);
                },
                error: function(statut) {
                    console.log("500 poly", urlPlatform, statut);
                    $(".put-3d-not-possible").addClass("actif");

                    setTimeout(function() {
                        $(".put-3d-not-possible").removeClass("actif");
                    }, 5000);
                }
            });
        }
    }

    /**
     * Print Objects infos
     */
    function PrintObjects() {
        var json = GetCompleteJSON();
        console.log(json);
    }

    /**
     * Reset Translation
     */
    function RstT() {
        actualObject.position.set(
            initialPosition.x,
            initialPosition.y,
            initialPosition.z
        );
    }

    /**
     * Reset Rotation
     */
    function RstR() {
        actualObject.quaternion.set(
            initialRotation.x,
            initialRotation.y,
            initialRotation.z,
            initialRotation.w
        );
        RotNone();
        // document.getElementById("None").checked = true;
    }

    /**
     * Reset scale
     */
    function RstS() {
        actualObject.scale.set(initialScale.x, initialScale.y, initialScale.z);
    }

    /**
     * Set for infinite rotation around X axis
     */
    function RotX() {
        rot(0.05, 0, 0);
        previousRot = "X";
    }

    /**
     * Set for infinite rotation around Y axis
     */
    function RotY() {
        rot(0, 0.05, 0);
        previousRot = "Y";
    }

    /**
     * Set for infinite rotation around Z axis
     */
    function RotZ() {
        rot(0, 0, 0.05);
        previousRot = "Z";
    }

    /**
     * Unset infinite rotation
     */
    function RotNone() {
        rot(0, 0, 0);
        previousRot = "None";
        // boiengStatus = false;
        $(".poly-animation-btn-boeing").removeClass("active");
    }

    function rot(x, y, z) {
        if (previousRot == "None") {
            rotationBeforeInfinite = new THREE.Quaternion().copy(
                actualObject.quaternion
            );
        }
        if (rotationBeforeInfinite != null) {
            actualObject.quaternion.set(
                rotationBeforeInfinite.x,
                rotationBeforeInfinite.y,
                rotationBeforeInfinite.z,
                rotationBeforeInfinite.w
            );
        }
        rotationBeforeInfinite = new THREE.Quaternion().copy(
            actualObject.quaternion
        );

        rotX = x;
        rotY = y;
        rotZ = z;
    }

    function Boeing() {
        translationBeforeBoeing = new THREE.Vector3().copy(
            actualObject.position
        );
        isBoeing = 1;
    }

    function BoeingNone() {
        if (translationBeforeBoeing != null) {
            actualObject.position.y = translationBeforeBoeing.y;
        } else {
            translationBeforeBoeing = new THREE.Vector3().copy(
                actualObject.position
            );
        }
        isBoeing = 0;
    }

    var camPos = new THREE.Vector3().copy(camera.position);
    //var camRot = new THREE.Quaternion().copy(camera.quaternion);
    var targetPos = new THREE.Vector3().copy(camera.position);
    //var targetRot = new THREE.Quaternion().copy(camera.quaternion);

    var movementOrientation = 1;
    var delay = 0;
    var isBoeing = 0;

    var isMoving = false;

    function animate() {
        var markerStatus = $("#marker-position").text();

        // Infinite Rotation
        // retrive aimation
        var speed = GetSpeed();

        if (actualObject != null) {
            // Rotate around World axis
            actualObject.rotateOnWorldAxis(
                new THREE.Vector3(1, 0, 0),
                rotX * speed * 0.0174533
            );
            actualObject.rotateOnWorldAxis(
                new THREE.Vector3(0, 1, 0),
                rotY * speed * 0.0174533
            );
            actualObject.rotateOnWorldAxis(
                new THREE.Vector3(0, 0, 1),
                rotZ * speed * 0.0174533
            );

            if (markerStatus == "Mur") {
                actualObject.position.z +=
                    isBoeing * movementOrientation * 0.006;
            } else {
                actualObject.position.y +=
                    isBoeing * movementOrientation * 0.006;
            }
        }

        for (var [key, value] of objects) {
            if (actualModel != key) {
                value.scaler.rotateOnWorldAxis(
                    new THREE.Vector3(1, 0, 0),
                    value.rotX * value.speed * 0.0174533
                );
                value.scaler.rotateOnWorldAxis(
                    new THREE.Vector3(0, 1, 0),
                    value.rotY * value.speed * 0.0174533
                );
                value.scaler.rotateOnWorldAxis(
                    new THREE.Vector3(0, 0, 1),
                    value.rotZ * value.speed * 0.0174533
                );

                if (markerStatus == "Mur") {
                    value.scaler.position.z +=
                        value.isBoeing * movementOrientation * 0.006;
                } else {
                    value.scaler.position.y +=
                        value.isBoeing * movementOrientation * 0.006;
                }
            }
        }

        if (++delay % 40 == 0) movementOrientation = -movementOrientation;

        // Camra switching
        if (isMoving) {
            camPos.lerp(targetPos, 0.05);
            camera.position.copy(camPos);

            //camRot.slerp(targetRot, 0.05);
            //camera.quaternion.copy(camRot);

            camera.lookAt(0, 1.5, 0);
            if (camera.position.distanceTo(targetPos) < 0.002) {
                isMoving = false;
            }
        }

        if (mixer) mixer.update(clock.getDelta());

        gifs.forEach(mat => {
            mat.map.needsUpdate = true;
            mat.displacementScale = 5;
            mat.displacementMap.needsUpdate = true;
        });

        requestAnimationFrame(animate);
        render();
    }

    function GetRotationAxis() {
        if (rotX != 0) {
            return "X";
        } else if (rotY != 0) {
            return "Y";
        } else if (rotZ != 0) {
            return "Z";
        } else {
            return "None";
        }
    }

    function GetSpeed() {
        return document.getElementById("speed").value;
    }

    function SetSpeed(val) {
        document.getElementById("speed").value = val;
    }

    animate();

    if (markerTexturePath) {
        ChangeTexture(markerTexturePath);
    }

    function ChangeTexture(texture) {
        markerTexturePath = texture;
        texture = new THREE.TextureLoader().load(texture, function() {
            render();
        });
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;

        texture.repeat.set(1, 1);

        mat = new THREE.MeshBasicMaterial({ map: texture });

        plane.material = mat;
        plane.material.side = THREE.DoubleSide;
    }

    //LOADERS
    var mixer;
    var clock = new THREE.Clock();

    /**
     * Load a 3DS file
     */
    function Load3DS(
        filePath,
        texturePath,
        fileName,
        isLoaded,
        idBack,
        callBack = null
    ) {
        fileName = fileName.toLowerCase();

        var loaderTexture = new THREE.TextureLoader();
        var texture = loaderTexture.load(texturePath);

        var loader = new THREE.TDSLoader();

        actualUrl = filePath;

        // loader.setPath(texturePath);
        loader.load(filePath, function(object) {
            box = new THREE.Box3();
            box.setFromObject(object);

            // Recenter Object
            var center = box.getCenter();
            //center.y = box.min.y;
            object.position.sub(center);

            object.traverse(function(child) {
                if (child instanceof THREE.Mesh) {
                    child.material.map = texture;
                }
            });

            // Rescale Object
            var scaler = new THREE.Group();
            scaler.add(object);
            // console.log(box.getSize());
            scaler.scale.setScalar(8 / box.getSize().length());

            container.add(scaler);

            // Set actual Model and transforming object variables
            actualModel = object;
            actualObject = scaler;

            if (isLoaded) {
                createThumbnailTextEdit(
                    fileName,
                    actualModel,
                    actualModel.id,
                    idBack
                );
            } else {
                createThumbnailText(fileName, actualModel, actualModel.id);
            }

            // Initial informations on object for Reset
            initialPosition = new THREE.Vector3().copy(scaler.position);
            initialScale = new THREE.Vector3().copy(scaler.scale);
            initialRotation = new THREE.Quaternion().copy(scaler.quaternion);

            RotNone();
            translationBeforeBoeing = initialPosition;
            BoeingNone();

            var value = {
                url: actualUrl,
                author: fileName,
                name: companyName,
                scaler: scaler,
                initPos: initialPosition,
                initScale: initialScale,
                initRot: initialRotation,
                RotBeforeInf: rotationBeforeInfinite,
                previousRot: previousRot,
                rotX: rotX,
                rotY: rotY,
                rotZ: rotZ,
                speed: 0,
                extension: "3ds",
                texturePath: texturePath,
                isBoeing: isBoeing,
                transBeforeBoeing: translationBeforeBoeing,
                isOld: true,
                box: box,
                idBack: idBack
            };
            objects.set(object, value);

            // console.log(value);

            // Attach Gizmo transform control to object
            control.attach(scaler);
            container.add(control);

            $(".spinner-load-model").css("display", "none");

            if (callBack) callBack();

            render();
        });
    }

    /**
     * Load a Collada ('DAE') file
     */
    function LoadCollada(
        filePath,
        texturePath,
        fileName,
        isLoaded,
        idBack,
        callBack = null
    ) {
        fileName = fileName.toLowerCase();

        var loaderTexture = new THREE.TextureLoader();
        var texture = loaderTexture.load(texturePath);
        var loader = new THREE.ColladaLoader();

        actualUrl = filePath;

        // loader.setPath(texturePath);
        loader.load(filePath, function(collada) {
            var object = collada.scene;

            object.traverse(function(node) {
                if (node.isMesh) {
                    node.material.map = texture;
                }
            });

            box = new THREE.Box3();
            box.setFromObject(object);

            // Recenter Object
            var center = box.getCenter();
            //center.y = box.min.y;
            object.position.sub(center);

            // Rescale Object
            var scaler = new THREE.Group();
            scaler.add(object);
            // console.log(box.getSize());
            scaler.scale.setScalar(8 / box.getSize().length());

            container.add(scaler);

            // Set actual Model and transforming object variables
            actualModel = object;
            actualObject = scaler;

            if (isLoaded) {
                createThumbnailTextEdit(
                    fileName,
                    actualModel,
                    actualModel.id,
                    idBack
                );
            } else {
                createThumbnailText(fileName, actualModel, actualModel.id);
            }

            console.log("LoadCollada() >> actual Object: " + actualObject);

            // Initial informations on object for Reset
            initialPosition = new THREE.Vector3().copy(scaler.position);
            initialScale = new THREE.Vector3().copy(scaler.scale);
            initialRotation = new THREE.Quaternion().copy(scaler.quaternion);

            RotNone();
            translationBeforeBoeing = initialPosition;
            BoeingNone();

            var value = {
                url: actualUrl,
                author: fileName,
                name: companyName,
                scaler: scaler,
                initPos: initialPosition,
                initScale: initialScale,
                initRot: initialRotation,
                RotBeforeInf: rotationBeforeInfinite,
                previousRot: previousRot,
                rotX: rotX,
                rotY: rotY,
                rotZ: rotZ,
                speed: 0,
                extension: "dae",
                texturePath: texturePath,
                isBoeing: isBoeing,
                transBeforeBoeing: translationBeforeBoeing,
                isOld: true,
                box: box,
                idBack: idBack
            };
            objects.set(object, value);

            // Attach Gizmo transform control to object
            control.attach(scaler);
            container.add(control);

            $(".spinner-load-model").css("display", "none");

            if (callBack) callBack();

            render();
        });
    }

    /**
     * Load a FBX file
     */
    function LoadFBX(filePath, fileName, isLoaded, idBack, callBack = null) {
        fileName = fileName.toLowerCase();

        var loader = new THREE.FBXLoader();
        var actualUrl = filePath;

        loader.load(
            actualUrl,
            function(object) {
                box = new THREE.Box3();
                box.setFromObject(object);

                // Recenter Object
                var center = box.getCenter();
                //center.y = box.min.y;
                object.position.sub(center);

                // Rescale Object
                var scaler = new THREE.Group();
                scaler.add(object);
                // console.log(box.getSize());
                scaler.scale.setScalar(8 / box.getSize().length());

                container.add(scaler);

                // Set actual Model and transforming object variables
                actualModel = object;
                actualObject = scaler;

                if (isLoaded) {
                    createThumbnailTextEdit(
                        fileName,
                        actualModel,
                        actualModel.id,
                        idBack
                    );
                } else {
                    createThumbnailText(fileName, actualModel, actualModel.id);
                }

                // Initial informations on object for Reset
                initialPosition = new THREE.Vector3().copy(scaler.position);
                initialScale = new THREE.Vector3().copy(scaler.scale);
                initialRotation = new THREE.Quaternion().copy(
                    scaler.quaternion
                );

                RotNone();
                translationBeforeBoeing = initialPosition;
                BoeingNone();

                var value = {
                    url: actualUrl,
                    author: fileName,
                    name: companyName,
                    scaler: scaler,
                    initPos: initialPosition,
                    initScale: initialScale,
                    initRot: initialRotation,
                    RotBeforeInf: rotationBeforeInfinite,
                    previousRot: previousRot,
                    rotX: rotX,
                    rotY: rotY,
                    rotZ: rotZ,
                    isBoeing: isBoeing,
                    transBeforeBoeing: translationBeforeBoeing,
                    speed: 0,
                    extension: "fbx",
                    isOld: false,
                    box: box,
                    idBack: idBack
                };
                objects.set(object, value);

                // Attach Gizmo transform control to object
                control.attach(scaler);
                container.add(control);

                $(".spinner-load-model").css("display", "none");

                if (callBack) callBack();

                render();
                $(".spinner-bg").fadeOut();
                $(".savePoly").fadeOut();
            },
            () => {
                $(".spinner-bg").fadeIn();
                $(".savePoly").fadeIn();
            },
            error => {
                console.log(error);
            }
        );
    }

    var texture;

    /**
     * Load an OBJ/[MTL] file
     * Used to pre-load MTL before OBJ in case there is one
     */
    function LoadOBJ(
        objPath,
        mtlPath,
        texturePath,
        fileName,
        isLoaded,
        idBack,
        callBack
    ) {
        fileName = fileName.toLowerCase();

        var mtlLoader = new THREE.MTLLoader();

        if (texturePath) {
            var loaderImage = new THREE.ImageLoader();
            loaderImage.load(texture, function(image) {
                texture.image = image;
                texture.needsUpdate = true;
                texture.wrapS = THREE.RepeatWrapping;
                texture.wrapT = THREE.RepeatWrapping;
                texture.repeat.set(1, 1);
            });
        }

        if (texturePath || mtlPath) {
            // mtlloader.setTexturePath(texture);
            if (texturePath) {
                mtlLoader.load(texturePath, function(materials) {
                    materials.preload();
                    OBJLoading(
                        objPath,
                        materials,
                        texturePath,
                        null,
                        fileName,
                        isLoaded,
                        idBack,
                        callBack
                    );
                });
            } else {
                mtlLoader.load(mtlPath, function(materials) {
                    materials.preload();
                    OBJLoading(
                        objPath,
                        materials,
                        null,
                        mtlPath,
                        fileName,
                        isLoaded,
                        idBack,
                        callBack
                    );
                });
            }
        } else {
            OBJLoading(objPath, null, null, null, fileName, idBack, callBack);
        }

        $(".spinner-load-model").css("display", "none");
    }

    /**
     * Load OBJ File
     */
    function OBJLoading(
        objPath,
        mat,
        texturePath,
        matPath,
        fileName,
        isLoaded,
        idBack,
        callBackLoadingModel
    ) {
        var loader = new THREE.OBJLoader();

        actualUrl = objPath;

        if (mat != null) loader.setMaterials(mat);

        loader.load(actualUrl, function(object) {
            box = new THREE.Box3();
            box.setFromObject(object);

            // Recenter Object
            var center = box.getCenter();
            //center.y = box.min.y;
            object.position.sub(center);

            // if(!matPath) {
            object.traverse(function(child) {
                if (child instanceof THREE.Mesh) {
                    child.material.map = texture;
                }
            });
            // }

            // Rescale Object
            var scaler = new THREE.Group();
            scaler.add(object);
            // console.log(box.getSize());
            scaler.scale.setScalar(8 / box.getSize().length());

            container.add(scaler);

            // Set actual Model and transforming object variables
            actualModel = object;
            actualObject = scaler;

            if (isLoaded) {
                createThumbnailTextEdit(
                    fileName,
                    actualModel,
                    actualModel.id,
                    idBack
                );
            } else {
                createThumbnailText(fileName, actualModel, actualModel.id);
            }

            // Initial informations on object for Reset
            initialPosition = new THREE.Vector3().copy(scaler.position);
            initialScale = new THREE.Vector3().copy(scaler.scale);
            initialRotation = new THREE.Quaternion().copy(scaler.quaternion);

            RotNone();
            translationBeforeBoeing = initialPosition;
            BoeingNone();

            var value = {
                url: actualUrl,
                author: fileName,
                name: companyName,
                scaler: scaler,
                initPos: initialPosition,
                initScale: initialScale,
                initRot: initialRotation,
                RotBeforeInf: rotationBeforeInfinite,
                previousRot: previousRot,
                rotX: rotX,
                rotY: rotY,
                rotZ: rotZ,
                speed: 0,
                extension: "obj",
                mtl: matPath,
                texturePath: texturePath,
                isBoeing: isBoeing,
                transBeforeBoeing: translationBeforeBoeing,
                isOld: null,
                box: box,
                idBack: idBack
            };
            objects.set(object, value);

            // Attach Gizmo transform control to object
            control.attach(scaler);
            container.add(control);

            if (callBackLoadingModel) callBackLoadingModel();

            render();
        });
    }

    //Load Image
    function LoadImage(imagePath, fileName, isLoaded, idBack, callBack = null) {
        fileName = fileName.toLowerCase();

        //Créer un plan
        var texture, mat;
        var supGif, gifcanvas;

        if (fileName.includes(".gif")) {
            extension = "gif";
            console.log("EXTENSION IMAGE : " + extension);
        } else if (fileName.includes(".png")) {
            extension = "png";
            console.log("EXTENSION IMAGE : " + extension);
        } else if (fileName.includes(".jpg") || fileName.includes(".jpeg")) {
            extension = "jpg";
            console.log("EXTENSION IMAGE : " + extension);
        }

        if (!fileName.includes(".gif")) {
            texture = new THREE.TextureLoader().load(imagePath);
            texture.wrapS = THREE.RepeatWrapping;
            texture.wrapT = THREE.RepeatWrapping;

            texture.repeat.set(1, 1);
            if (fileName.includes("png"))
                mat = new THREE.MeshBasicMaterial({
                    map: texture,
                    alphaTest: 0.5,
                    transparent: true,
                    side: THREE.DoubleSide
                });
            else mat = new THREE.MeshBasicMaterial({ map: texture });
        } else {
            var img = document.createElement("img");
            img.src = imagePath;
            img.id = imagePath;

            document.body.appendChild(img);

            supGif = new SuperGif({ gif: document.getElementById(imagePath) });
            supGif.load();
            gifcanvas = supGif.get_canvas();

            mat = new THREE.MeshBasicMaterial();
            mat.map = new THREE.Texture(gifcanvas);
            mat.displacementMap = mat.map;

            gifs.push(mat);
        }

        var geometry = new THREE.PlaneGeometry(5, 5);

        var plane = new THREE.Mesh(geometry, mat);
        plane.rotateX(1.5708);
        plane.rotateY(1.5708 * 2);
        plane.position.y += 0.02;

        actualUrl = imagePath;
        var scaler = new THREE.Group();
        scaler.name = "scaler";
        scaler.add(plane);

        container.add(scaler);

        actualModel = plane;
        actualObject = scaler;

        if (isLoaded) {
            createThumbnailTextEdit(
                fileName,
                actualModel,
                actualModel.id,
                idBack
            );
        } else {
            createThumbnailText(fileName, actualModel, actualModel.id);
        }

        initialPosition = new THREE.Vector3().copy(scaler.position);
        initialScale = new THREE.Vector3().copy(scaler.scale);
        initialRotation = new THREE.Quaternion().copy(scaler.quaternion);

        RotNone();
        translationBeforeBoeing = initialPosition;
        BoeingNone();

        //mettre le plan dans les objets
        var value = {
            url: actualUrl,
            author: fileName,
            name: companyName,
            scaler: scaler,
            initPos: initialPosition,
            initScale: initialScale,
            initRot: initialRotation,
            RotBeforeInf: rotationBeforeInfinite,
            previousRot: previousRot,
            rotX: rotX,
            rotY: rotY,
            rotZ: rotZ,
            speed: 0,
            extension: extension,
            isBoeing: isBoeing,
            transBeforeBoeing: translationBeforeBoeing,
            isOld: null,
            idBack: idBack
        };
        objects.set(plane, value);

        control.attach(scaler);
        container.add(control);

        $(".spinner-load-model").css("display", "none");

        renderer.sortObjects = false;

        if (callBack) callBack();

        render();
    }

    function LoadModelOnClickImage(actualModel) {
        saveModelInfo();
        majModel(actualModel);
    }

    if (polyJson.length > 0) {
        loadAllAssets(polyJson);
    } else {
        console.log("pas de json");
    }

    // THUMBNAILS
    function createThumbnailsEdit(imageUrl, actualModel, id, idBack) {
        $(document.createElement("img"))
            .attr({
                src: imageUrl,
                id: "thumbnailActif",
                class: id + " thumbnailImage active",
                modelId: id
            })
            .data("actualModel", actualModel)
            .appendTo($(".models-actif"));

        $(document.createElement("span"))
            .html('<i class="fas fa-trash-alt"></i>')
            .attr({
                class: idBack + " thumbnailTrash active",
                modelId: id
            })
            .insertAfter($("." + id + ".thumbnailImage.active"));
    }

    function createThumbnailTextEdit(fileName, actualModel, id, idBack) {
        $(".thumbnailText").removeClass("active");
        $(".thumbnailImage").removeClass("active");
        $(".thumbnailTrash").removeClass("active");

        fileName = fileName.toLowerCase();

        $(document.createElement("h3"))
            .text(fileName)
            .attr({
                id: "thumbnailActif",
                class: "thumbnailText active",
                modelId: id
            })
            .data("actualModel", actualModel)
            .appendTo($(".models-actif"));

        $(document.createElement("span"))
            .html('<i class="fas fa-trash-alt"></i>')
            .attr({
                class: idBack + " thumbnailTrash thumbText active",
                modelId: id
            })
            .insertAfter($(".thumbnailText.active"));
    }

    function createThumbnailText(fileName, actualModel, id) {
        $(".thumbnailText").removeClass("active");
        $(".thumbnailImage").removeClass("active");
        $(".thumbnailTrash").removeClass("active");

        fileName = fileName.toLowerCase();

        $(document.createElement("h3"))
            .text(fileName)
            .attr({
                id: "thumbnailActif",
                class: "thumbnailText active",
                modelId: id
            })
            .data("actualModel", actualModel)
            .appendTo($(".models-actif"));

        $(document.createElement("span"))
            .html('<i class="fas fa-trash-alt"></i>')
            .attr({
                class: "thumbnailTrash thumbText active",
                modelId: id
            })
            .insertAfter($(".thumbnailText.active"));

        asset_name.textContent = fileName;
        asset_author.textContent = companyName;
    }

    function createThumbnails(imageUrl, actualModel, id) {
        $(document.createElement("img"))
            .attr({
                src: imageUrl,
                id: "thumbnailActif",
                class: "thumbnailImage active",
                modelId: id
            })
            .data("actualModel", actualModel)
            .appendTo($(".models-actif"));

        $(document.createElement("span"))
            .html('<i class="fas fa-trash-alt"></i>')
            .attr({
                class: "thumbnailTrash active",
                modelId: id
            })
            .insertAfter($(".thumbnailImage.active"));
    }

    var textureJPG = null;
    var textureMTL = null;

    function readFile3D(input) {
        if (input.files && input.files[0]) {
            var readerFile3D = new FileReader();

            // $('.spinner-load-model').css('display', 'inline-block');

            for (var i = 0; i < input.files.length; i++) {
                var fileName = input.files[i].name;
                // var userName = ""; API GET USERNAME du compte
                // asset_author.textContent = userName;

                fileName = fileName.toUpperCase();

                var fileFBX = ".FBX";
                var file3DS = ".3DS";
                var fileDAE = ".DAE";
                var fileOBJ = ".OBJ";
                var fileMTL = ".MTL";
                var filePNG = ".PNG";
                var fileJPG = ".JPG";
                var fileGIF = ".GIF";

                if (
                    !fileName.includes(fileFBX) &&
                    !fileName.includes(fileOBJ) &&
                    !fileName.includes(file3DS) &&
                    !fileName.includes(fileDAE) &&
                    !fileName.includes(fileOBJ) &&
                    !fileName.includes(filePNG) &&
                    !fileName.includes(fileJPG) &&
                    !fileName.includes(fileGIF)
                ) {
                    $(".wrong-files").addClass("actif");

                    setTimeout(function() {
                        $(".wrong-files").removeClass("actif");
                    }, 5000);

                    $(".delete").on("click", function() {
                        $(".wrong-files").removeClass("actif");
                    });

                    // $('.spinner-load-model').css('display', 'none');
                } else if (fileName.includes(fileFBX)) {
                    readerFile3D.onload = function(e) {
                        resultFile = readerFile3D.result;
                        resultFileName = fileName;
                        // LoadFBX(readerFile3D.result, fileName, null);
                        finalName = fileName;
                    };
                } else if (fileName.includes(file3DS)) {
                    readerFile3D.onload = function(e) {
                        resultFile = readerFile3D.result;
                        resultFileName = fileName;
                        // Load3DS(readerFile3D.result, textureJPG, fileName, null);
                        finalName = fileName;
                    };
                } else if (fileName.includes(fileDAE)) {
                    readerFile3D.onload = function(e) {
                        resultFile = readerFile3D.result;
                        resultFileName = fileName;

                        // LoadCollada(readerFile3D.result, textureJPG, fileName, null);
                        finalName = fileName;
                    };
                } else if (fileName.includes(fileOBJ) && textureMTL) {
                    readerFile3D.onload = function(e) {
                        resultFile = readerFile3D.result;
                        resultFileName = fileName;
                        textureMTL = null;

                        // LoadOBJ(readerFile3D.result, textureMTL, null, fileName, null, null, null);

                        // finalName = fileName;
                    };
                } else if (fileName.includes(fileOBJ) && !textureMTL) {
                    readerFile3D.onload = function(e) {
                        resultFile = readerFile3D.result;
                        resultFileName = fileName;

                        // LoadOBJ(readerFile3D.result, '', textureJPG, fileName, null, null, null);
                        finalName = fileName;
                    };
                } else if (
                    fileName.includes(fileJPG) ||
                    fileName.includes(filePNG) ||
                    fileName.includes(fileGIF)
                ) {
                    readerFile3D.onload = function(e) {
                        resultFile = readerFile3D.result;
                        resultFileName = fileName;
                        // LoadImage(readerFile3D.result, fileName, null);
                    };
                }
            }

            if (
                fileName.includes(fileJPG) ||
                fileName.includes(filePNG) ||
                fileName.includes(fileGIF)
            ) {
                $(".add-image .label-after").html(fileName);
                $(".add-image .label-before").html("Changer votre image");
                $(".add-image .label").addClass("active");
            } else {
                $(".add-ownFile3D .label-after").html(fileName);
                $(".add-ownFile3D .label-before").html(
                    "Changer votre objet 3D"
                );
                $(".add-ownFile3D .label").addClass("active");
            }

            readerFile3D.readAsDataURL(input.files[0]);
        }
    }

    $(".model3d-import").click(function() {
        $(".editor-import-modal").removeClass("is-active");

        $(".add-ownFile3D .label-after").empty();
        $(".add-ownFile3D .label-before").html(
            "Importer votre propre objet 3D"
        );
        $(".add-ownFile3D .label").removeClass("active");

        $(".add-ownTexture .label-after").empty();
        $(".add-ownTexture .label-before").html(
            "Importer votre propre texture"
        );
        $(".add-ownTexture .label").removeClass("active");

        $(".add-image .label-after").empty();
        $(".add-image .label-before").html("Importer votre propre image");
        $(".add-image .label").removeClass("active");

        if (resultFileName) {
            if (
                resultFileName.includes(".JPG") &&
                resultFileName.includes(".PNG") &&
                resultFileName.includes(".GIF")
            ) {
                $(".wrong-files").addClass("actif");

                setTimeout(function() {
                    $(".wrong-files").removeClass("actif");
                }, 5000);

                $(".delete").on("click", function() {
                    $(".wrong-files").removeClass("actif");
                });

                $(".spinner-load-model").css("display", "none");
            }

            if (resultFileName.includes(".OBJ") && textureMTL) {
                LoadOBJ(
                    resultFile,
                    textureMTL,
                    null,
                    resultFileName,
                    null,
                    null,
                    null
                );
            } else if (resultFileName.includes(".OBJ") && !textureMTL) {
                LoadOBJ(
                    resultFile,
                    "",
                    textureJPG,
                    resultFileName,
                    null,
                    null,
                    null
                );
            } else if (resultFileName.includes(".FBX")) {
                LoadFBX(resultFile, resultFileName, null);
            } else if (resultFileName.includes(".3DS")) {
                Load3DS(resultFile, textureJPG, resultFileName, null);
            } else if (resultFileName.includes(".DAE")) {
                LoadCollada(resultFile, textureJPG, resultFileName, null);
            }
        }
    });

    $(".import-btn-send").on("click", ".image-import", function() {
        $(".add-ownFile3D .label-after").empty();
        $(".add-ownFile3D .label-before").html(
            "Importer votre propre objet 3D"
        );
        $(".add-ownFile3D .label").removeClass("active");

        $(".add-ownTexture .label-after").empty();
        $(".add-ownTexture .label-before").html(
            "Importer votre propre texture"
        );
        $(".add-ownTexture .label").removeClass("active");

        $(".add-image .label-after").empty();
        $(".add-image .label-before").html("Importer votre propre image");
        $(".add-image .label").removeClass("active");

        if (container.children.length > 3) {
            $(".limit-model").addClass("actif");

            setTimeout(function() {
                $(".limit-model").removeClass("actif");
            }, 5000);

            $(".delete").on("click", function() {
                $(".limit-model").removeClass("actif");
            });
        } else {
            if (resultFileName) {
                if (
                    !resultFileName.includes(".JPG") &&
                    !resultFileName.includes(".PNG") &&
                    !resultFileName.includes(".GIF")
                ) {
                    $(".wrong-files").addClass("actif");

                    setTimeout(function() {
                        $(".wrong-files").removeClass("actif");
                    }, 5000);

                    $(".delete").on("click", function() {
                        $(".wrong-files").removeClass("actif");
                    });

                    $(".spinner-load-model").css("display", "none");
                }

                if (
                    resultFileName.includes(".JPG") ||
                    resultFileName.includes(".PNG") ||
                    resultFileName.includes(".GIF")
                ) {
                    LoadImage(resultFile, resultFileName, null);
                }
            }
        }
    });

    function readTexture(input) {
        if (input.files && input.files[0]) {
            var readerFileJPG = new FileReader();

            for (var i = 0; i < input.files.length; i++) {
                fileName = input.files[i].name;

                $(".add-ownTexture .label-after").html(fileName);
                $(".add-ownTexture .label-before").html(
                    "Changer votre texture"
                );
                $(".add-ownTexture .label").addClass("active");

                fileName = fileName.toUpperCase();

                var fileJPG = ".JPG";
                var filePNG = ".PNG";
                var fileMTL = ".MTL";

                if (
                    !fileName.includes(fileJPG) &&
                    !fileName.includes(filePNG) &&
                    !fileName.includes(fileMTL)
                ) {
                    $(".wrong-files").addClass("actif");

                    setTimeout(function() {
                        $(".wrong-files").removeClass("actif");
                    }, 5000);

                    $(".delete").on("click", function() {
                        $(".wrong-files").removeClass("actif");
                    });
                } else if (
                    fileName.includes(fileJPG) ||
                    fileName.includes(filePNG)
                ) {
                    readerFileJPG.onload = function(e) {
                        textureJPG = readerFileJPG.result;
                    };
                } else if (fileName.includes(fileMTL)) {
                    readerFileJPG.onload = function(e) {
                        textureMTL = readerFileJPG.result;
                    };
                }
            }

            readerFileJPG.readAsDataURL(input.files[0]);
        }
    }

    $("#ownTexture").change(function() {
        readTexture(this);
    });

    $("#ownFile3D").change(function() {
        if (container.children.length > 3) {
            $(".limit-model").addClass("actif");

            setTimeout(function() {
                $(".limit-model").removeClass("actif");
            }, 5000);

            $(".delete").on("click", function() {
                $(".limit-model").removeClass("actif");
            });
        } else {
            readFile3D(this);
        }
    });

    $("#ownFileImage").change(function() {
        if (container.children.length > 3) {
            $(".limit-model").addClass("actif");

            setTimeout(function() {
                $(".limit-model").removeClass("actif");
            }, 5000);

            $(".delete").on("click", function() {
                $(".limit-model").removeClass("actif");
            });
        } else {
            readFile3D(this);
        }
    });

    $(".color-icon").click(function() {
        $(".color-icon").removeClass("active");
        $(this).addClass("active");

        colorSelected = $(this).css("backgroundColor");
        haveColor = true;
        $(".color-preview").css("backgroundColor", colorSelected);
        newHex = rgb2hex(colorSelected);
        $(".hexcolor").val(newHex);
        isBoxColor = true;
        if (isBoxColor && isBoxAnimation) {
            animationSelected = $(".animation-box.active").attr("animation");
            canOpenBoxEditor();
            saveEdit(
                newHex,
                boxColor,
                ".box-color-block #save-edit",
                ".box-color-block .anim-open-header .fa-save"
            );
        }
    });

    //Function to convert hex format to a rgb color (detect backgroundColor give a hex format)
    function rgb2hex(rgb) {
        rgb = rgb.match(
            /^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i
        );
        return rgb && rgb.length === 4
            ? "#" +
                  ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
                  ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
                  ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2)
            : "";
    }

    $(".hexcolor").on("change", function() {
        $(".color-preview").css("backgroundColor", this.value);
        isBoxColor = true;

        if (this.value.includes("rgb")) {
            newHex = rgb2hex(this.value);
            $(".hexcolor").val(newHex);
        }

        if (isBoxColor && isBoxAnimation) {
            animationSelected = $("animation-box.active").attr("animation");
            canOpenBoxEditor();
            saveEdit(
                newHex,
                boxColor,
                ".box-color-block #save-edit",
                ".box-color-block .anim-open-header .fa-save"
            );
        }
    });

    $(".animation-box").click(function() {
        $(".animation-box").removeClass("active");
        $(this).addClass("active");
        animationSelected = $(this).attr("animation");
        isBoxAnimation = true;
        if (isBoxColor && isBoxAnimation) {
            canOpenBoxEditor();
            saveEdit(
                animationSelected,
                boxAnimation,
                ".anim-open-block #save-edit",
                ".anim-open-block .anim-open-header .fa-save"
            );
        }
    });

    function removeBox(markerImage = null) {
        scene.remove(planeleft);
        scene.remove(planeRight);
        scene.remove(planeBack);
        scene.remove(planeTop);
        scene.remove(planeArrow);
        scene.remove(planeUser);

        ChangeTexture(
            markerImage ? markerImage : "../../modelEditor/marker.jpg"
        );
    }

    $(".btn-aug-box").click(function() {
        selectedChoice = "box";

        // $('.switch-pos').addClass('inactif');
        if (isBoxColor && isBoxAnimation) {
            if (markerTexturePath) {
                removeBox(markerTexturePath);
            } else {
                removeBox();
            }

            $("#marker-position").text("Sol");

            boxInit(newHex, animationSelected);
        }
    });

    $(".btn-aug-float").click(function() {
        selectedChoice = "notBox";

        // $('.switch-pos').removeClass('inactif');

        if (markerTexturePath) {
            removeBox(markerTexturePath);
        } else {
            removeBox();
        }
    });
});
