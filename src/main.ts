import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";

// my imports
import axios from "axios";
import Buefy from "buefy";
import VCalendar from "v-calendar";
import "v-calendar/lib/v-calendar.min.css";
import VeeValidate, { Validator } from "vee-validate";
import VueCarousel from "vue-carousel";
import VueMoment from "vue-moment";
import vueSmoothScroll from "vue-smoothscroll";
import Scrollspy from "vue2-scrollspy";

import dictionary from "./error-dictionary";
import "./registerServiceWorker";

Vue.config.productionTip = false;

Vue.use(Buefy, axios, vueSmoothScroll, VueCarousel);
Vue.use(Scrollspy as any);
Vue.use(VCalendar as any);
Vue.use(VueMoment as any);
Vue.use(VeeValidate, {
    events: ""
});

Validator.localize(dictionary);

Vue.filter("byteConvert", (bytes: any, decimals: number) => {
    if (bytes === 0) {
        return "0 Bytes";
    }

    if (isNaN(parseFloat(bytes)) && !isFinite(bytes)) {
        return "Not an number";
    }

    const dm = decimals || 2;
    const sizes = ["Bytes", "Ko", "Mo", "Go", "To", "Po", "Eo", "Zo", "Yo"];
    const i = Math.floor(Math.log(bytes) / Math.log(1024));

    return parseFloat((bytes / Math.pow(1024, i)).toFixed(dm)) + " " + sizes[i];
});

new Vue({
    router,
    store,
    render: (h) => h(App)
}).$mount("#app");
