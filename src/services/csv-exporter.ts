/**
 * Exports the data object in parameter into a csv file that will be automatically downloaded by the user
 * @param data the data to export into a csv file
 * @param columnDelimiter the delimiter character used to delimit column in the CSV file
 * @param lineDelimiter  the delimiter character used to delimit lines in the CSV file.
 */
export function exportCsv(
    data: any,
    exportFileName: string = "export",
    columnDelimiter: string = ";",
    lineDelimiter: string = "\n"
) {
    let result: string = "";
    const keys = Object.keys(data[0]);

    // format CSV data into variable "result"
    result = "";
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach((item: any) => {
        let ctr = 0;
        keys.forEach((key) => {
            if (ctr > 0) {
                result += columnDelimiter;
            }

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    // make the client upload the csv data into a file
    result = "data:text/csv;charset=utf-16," + result;
    const csv = encodeURI(result);
    const link = document.createElement("a");
    link.setAttribute("href", csv);
    link.setAttribute("download", exportFileName);
    link.click();
    link.remove();
}
