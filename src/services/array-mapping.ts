/**
 * Maps an associative array to a real array
 * @param obj the associative array to map
 */
export default function arrayMapping<T>(obj: { [index: number]: T }): T[] {
    const result: T[] = [];
    Object.keys(obj).forEach((key: any) => {
        result[key as number] = obj[key];
    });
    return result;
}
