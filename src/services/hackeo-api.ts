import Address from "@/models/Address";
import Event from "@/models/Event";
import { HackeoResponse } from "@/models/HackeoResponse";
import HackeoStatsResponse from "@/models/HackeoStatsReponse";
import HttpError from "@/models/HttpError";
import Profile from "@/models/Profile";
import Axios, { AxiosInstance, AxiosResponse } from "axios";

class HackeoApi {
    private api: AxiosInstance;

    constructor() {
        let baseUrl;
        if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
            baseUrl = "http://127.0.0.1:8001";
        } else {
            baseUrl = "http://api.example.com";
        }
        this.api = Axios.create({
            baseURL: baseUrl,
            headers: {
                Accept: "application/json"
            }
        });
        const token = localStorage.getItem("token");
        if (token) {
            // tslint:disable-next-line:no-string-literal
            this.api.defaults.headers.common["Authorization"] =
                "Bearer " + token;
        }
    }

    /**
     * login action to authenticate the user
     * @param email
     * @param password
     */
    public async login(email: string, password: string): Promise<string> {
        return this.api
            .post("/api/login_check", {
                email,
                password
            })
            .then((response: AxiosResponse<any>) => {
                const token: string = response.data.token;
                // tslint:disable-next-line:no-string-literal
                this.api.defaults.headers.common["Authorization"] =
                    "Bearer " + token;
                return token;
            })
            .catch((reason) => {
                throw new HttpError(
                    reason.response.status,
                    reason.response.data.message,
                    reason.response.data
                );
            });
    }

    public clearHeader() {
        // tslint:disable-next-line:no-string-literal
        delete this.api.defaults.headers.common["Authorization"];
    }

    /**
     * gets the data of the user for the full platform
     */
    public async getUserData() {
        return this.api
            .get<HackeoResponse>("/api/data")
            .then((response: AxiosResponse<HackeoResponse>) => {
                return response.data;
            })
            .catch((reason) => {
                throw new HttpError(
                    reason.response.status,
                    reason.response.data.message,
                    reason.response.data
                );
            });
    }

    /**
     * Retrieves the stats related to the specified event
     * @param eventId the event related to the stats we want to retrieve.
     */
    public async getEventStatsData(eventId: number) {
        return this.api
            .get<HackeoStatsResponse>(`/api/events/${eventId}/stats`)
            .then((response: AxiosResponse<HackeoStatsResponse>) => {
                return response.data;
            })
            .catch((reason) => {
                throw new HttpError(
                    reason.response.status,
                    reason.response.data.message,
                    reason.response.data
                );
            });
    }

    public async putAddress(address: Address) {
        try {
            const resp: AxiosResponse<any> = await this.api.put(
                `/api/addresses/${address.id}`,
                {
                    city: address.city,
                    cp: address.cp,
                    lat: address.lat,
                    lng: address.lng,
                    alt: address.alt,
                    addressOne: address.addressOne,
                    addressTwo: address.addressTwo
                }
            );
            return resp.data;
        } catch (reason) {
            throw new HttpError(
                reason.response.status,
                reason.response.data,
                reason.response.data
            );
        }
    }

    public async putProfile(profile: Profile, address: Address) {
        try {
            const resp: AxiosResponse<any> = await this.api.put(
                `api/customers/${profile.id}`,
                {
                    firstName: profile.firstname,
                    lastName: profile.lastname,
                    email: profile.email,
                    companyName: profile.companyName,
                    phoneNumber: profile.phoneNumber,
                    companyAddress: {
                        addressOne: address.addressOne,
                        addressTwo: address.addressTwo,
                        city: address.city,
                        cp: address.cp
                    }
                }
            );

            return resp.data;
        } catch (reason) {
            throw new HttpError(
                reason.response.status,
                reason.response.data,
                reason.response.data
            );
        }
    }

    /**
     * API call to generate visitor token into the event specified by the id in parameter.
     * @param id the id of the events where the token will be generated.
     */
    public async generateVisitorToken(id: number) {
        try {
            const resp: AxiosResponse<any> = await this.api.post(
                `/api/events/${id}/tokens`
            );
            return {
                id: resp.data.id,
                token: resp.data.token
            }; // returns the token
        } catch (reason) {
            throw new HttpError(
                reason.response.status,
                reason.response.data,
                reason.response.data
            );
        }
    }

    /**
     * Retrieves the non-validated pictures of the specified event
     * @param eventId the id of the event to specify
     */
    public async getNonValidatedPictures(
        eventId: number
    ): Promise<Array<{ id: number; path: string }>> {
        try {
            const resp: AxiosResponse<any[]> = await this.api.get(
                `/api/media?status=1&journeyLine[exists]=true&journeyLine.journey.event.id=${eventId}`
            );
            return Object.values(resp.data).map((mediaResp) => {
                return {
                    id: mediaResp.id,
                    path: mediaResp.webPath
                };
            });
        } catch (reason) {
            console.log(reason);
            throw new HttpError(
                reason.response.status,
                reason.response.data,
                reason.response.data
            );
        }
    }

    /**
     * API call to accept all the specified picture's ids in the specified event
     * @param eventId the id of the event where the pictures will be accepted.
     * @param pictureIds The ids of the pictures to accept in the specified event
     */
    public async acceptPictures(eventId: number, pictureIds: number[]) {
        try {
            const resp: AxiosResponse<any> = await this.api.put(
                `/api/events/${eventId}/accept-pictures`,
                pictureIds
            );
        } catch (reason) {
            throw new HttpError(
                reason.response.status,
                reason.response.data,
                reason.response.data
            );
        }
    }

    /**
     * API call to refuse all the specified picture's ids in the specified event
     * @param eventId the id of the event where the pictures will be refused.
     * @param pictureIds ids of the pictures to refuse in the specified event
     */
    public async refusePictures(eventId: number, pictureIds: number[]) {
        try {
            const resp: AxiosResponse<any> = await this.api.put(
                `/api/events/${eventId}/refuse-pictures`,
                pictureIds
            );
        } catch (reason) {
            throw new HttpError(
                reason.response.status,
                reason.response.data,
                reason.response.data
            );
        }
    }

    /**
     * Sets the interceptor of response error.
     * In practice, this method is used in App.vue to dispatch a logout action when the bearer token is expired.
     * @param funct arrow function used when the API call intercepts an error
     */
    public setErrorResponseInterceptor(funct: (error: any) => any) {
        this.api.interceptors.response.use(undefined, funct);
    }

    /**
     * Performs a PUT call to the API to modify the specified event
     * @param event the event to modify
     */
    public async putEvent(event: Event): Promise<Event> {
        try {
            console.log(typeof event.eventEnd.date, event.eventStart.date);
            const resp: AxiosResponse<any> = await this.api.put(
                `/api/events/${event.id}`,
                {
                    name: event.name,
                    createdAt: event.createdAt.date,
                    eventStart: event.eventStart.date,
                    eventEnd: event.eventEnd.date,
                    imgModeration: event.imgModeration,
                    canUploadImg: event.canUploadImg,
                    beaconPopupDistance: event.beaconPopupDistance,
                    teamEnabled: event.isTeamEnabled,
                    locationRadius: event.locationRadius,
                    interfaceColor: event.interfaceColor,
                    markerCount: event.markerCount,
                    beaconEnabled: event.isBeaconEnabled,
                    scheduleEnabled: event.isScheduleEnabled,
                    charged: event.isCharged,
                    textStatus: event.status,
                    textDurationType: event.durationType
                }
            );
            event.lastUpdate = resp.data.lastUpdate;
            return event;
        } catch (reason) {
            throw new HttpError(
                reason.response.status,
                reason.response.data,
                reason.response.data
            );
        }
    }

    /**
     * API call to put the event specified by the eventId in parameter.
     * @param eventId the id of the event to put in test mode.
     */
    public async putEventTest(eventId: number) {
        try {
            const resp: AxiosResponse<any> = await this.api.put(
                `/api/events/${eventId}/test-mode`
            );
        } catch (reason) {
            throw new HttpError(
                reason.response.status,
                reason.response.data,
                reason.response.data
            );
        }
    }

    /**
     * API call to put the event specified by the eventId in parameter
     * @param eventId the id of the event to put online
     */
    public async putEventOnline(eventId: number) {
        try {
            const resp: AxiosResponse<any> = await this.api.put(
                `/api/events/${eventId}/online`
            );
        } catch (reason) {
            throw new HttpError(
                reason.response.status,
                reason.response.data,
                reason.response.data
            );
        }
    }
}
const hackeoApi: HackeoApi = new HackeoApi();
export default hackeoApi;
