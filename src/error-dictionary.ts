const dictionary = {
  en: {
    messages: {
      required: (field: string) => `Ce champ est obligatoire.`,
      min: (field: string, params: any) =>
        `Ce champ doit avoir au moins ${params[0]} caractères.`,
      max: (field: string, params: any) =>
        `Ce champ peut avoir ${params[0]} caractères maximum.`,
      email: (field: string, params: any) =>
        `Ce champ doit être un email valide.`
    }
  }
};

export default dictionary;
