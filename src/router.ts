import { LoadingProgrammatic, Toast } from "buefy";
import Vue from "vue";
import Router, { Route } from "vue-router";
import Editor3D from "./components/Editor3D.vue";
import Profile from "./models/Profile";
import vuexStore, { parseJwt } from "./store/store";
import Account from "./views/Account.vue";
import Equipment from "./views/Beacon/Equipment.vue";
import ErrorPage from "./views/ErrorPage.vue";
import EventCode from "./views/Event/EventCode.vue";
import EventList from "./views/Event/EventList.vue";
import EventParameters from "./views/Event/EventParameters.vue";
import EventParcours from "./views/Event/EventParcours.vue";
import EventPictures from "./views/Event/EventPictures.vue";
import EventStart from "./views/Event/EventStart.vue";
import EventWaypoints from "./views/Event/EventWaypoints.vue";
import Faq from "./views/Faq/Faq.vue";
import FaqForm from "./views/Faq/FaqForm.vue";
import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
import OrderList from "./views/Order/OrderList.vue";
import StatsActivity from "./views/Stats/StatsActivity.vue";
import StatsDashboard from "./views/Stats/StatsDashboard.vue";
import StatsFeedback from "./views/Stats/StatsFeedbacks.vue";
import StatsPhotos from "./views/Stats/StatsPhotos.vue";
import StatsRanking from "./views/Stats/StatsRanking.vue";
import StatsResults from "./views/Stats/StatsResults.vue";
import StatsVisitors from "./views/Stats/StatsVisitors.vue";
import StatsWaypoint from "./views/Stats/StatsWaypoints.vue";
import StoryForm from "./views/Story/StoryForm.vue";
import StoryLibrary from "./views/Story/StoryLibrary.vue";
import StoryStore from "./views/Story/StoryStore.vue";
import Subscription from "./views/Subscription.vue";
import WaypointForm from "./views/Waypoint/WaypointForm.vue";

Vue.use(Router);

const loadStatsGuard = async (to: Route, from: Route, next: any) => {
    // tslint:disable-next-line:triple-equals
    if ((vuexStore.state as any).stats.eventId != to.params.id) {
        const loadingComponent = Vue.prototype.$loading.open({
            container: null
        });
        try {
            await vuexStore.dispatch("stats/loadStats", to.params.id);
            next();
        } catch (err) {
            console.log(err);
            Vue.prototype.$toast.open({
                message:
                    "Une erreur est survenue lors du chargement des statistiques. Veuillez réessayer.",
                type: "is-danger"
            });
            next(false);
        } finally {
            loadingComponent.close();
        }
    }
};

const router: Router = new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/",
            name: "home",
            component: Home
        },
        {
            path: "/login",
            name: "Login",
            component: Login,
            meta: {
                isAnonymous: true
            }
        },
        {
            path: "/account",
            name: "account",
            component: Account
        },
        {
            path: "/events",
            name: "EventList",
            component: EventList
        },
        {
            path: "/events/:id/start",
            name: "EventStart",
            component: EventStart
        },
        {
            path: "/events/:id/parcours",
            name: "EventParcours",
            component: EventParcours
        },
        {
            path: "/events/:id/pictures",
            name: "EventPictures",
            component: EventPictures
        },
        {
            path: "/events/:id/parameters",
            name: "EventParameters",
            component: EventParameters
        },
        {
            path: "/events/:id/waypoints",
            name: "EventWaypoints",
            component: EventWaypoints
        },
        {
            path: "/waypoints/:id/form",
            name: "WaypointForm",
            component: WaypointForm
        },
        {
            path: "/stories/store",
            name: "StoryStore",
            component: StoryStore
        },
        {
            path: "/stories/library",
            name: "StoryLibrary",
            component: StoryLibrary
        },
        {
            path: "/stories/form",
            name: "StoryForm",
            component: StoryForm
        },
        {
            path: "/beacons",
            name: "BeaconsList",
            component: Equipment
        },
        {
            path: "/events/:id/stats",
            name: "StatsDashboard",
            component: StatsDashboard,
            meta: {
                isStats: true
            }
        },
        {
            path: "/events/:id/stats/ranking",
            name: "StatsRanking",
            component: StatsRanking,
            meta: {
                isStats: true
            }
        },
        {
            path: "/events/:id/stats/pictures",
            name: "StatsPictures",
            component: StatsPhotos,
            meta: {
                isStats: true
            }
        },
        {
            path: "/events/:id/stats/feedbacks",
            name: "StatsFeedbacks",
            component: StatsFeedback,
            meta: {
                isStats: true
            }
        },
        {
            path: "/events/:id/stats/visitors",
            name: "StatsVisitors",
            component: StatsVisitors,
            meta: {
                isStats: true
            }
        },
        {
            path: "/events/:id/stats/waypoints",
            name: "StatsWaypoints",
            component: StatsWaypoint,
            meta: {
                isStats: true
            }
        },
        {
            path: "/events/:id/stats/results",
            name: "StatsResults",
            component: StatsResults,
            meta: {
                isStats: true
            }
        },
        {
            path: "/events/:id/stats/activity",
            name: "StatsActivity",
            component: StatsActivity,
            meta: {
                isStats: true
            }
        },
        {
            path: "/events/:id/realtime",
            name: "EventsRealtime",
            component: Login
        },
        {
            path: "/subscription",
            name: "Subscription",
            component: Subscription
        },
        {
            path: "/orders",
            name: "Orders",
            component: OrderList
        },
        {
            path: "/events/:id/codes",
            name: "EventsCodeList",
            component: EventCode
        },
        {
            path: "/faq",
            name: "Faq",
            component: Faq
        },
        {
            path: "/faq/form",
            name: "FaqForm",
            component: FaqForm
        },
        {
            path: "/editor3D",
            name: "Editor3D",
            component: Editor3D
        },
        {
            path: "/errors/:code",
            name: "ErrorPage",
            component: ErrorPage,
            meta: {
                isAnonymous: true
            }
        },
        {
            path: "*",
            redirect: "/errors/404",
            meta: {
                isAnonymous: true
            }
        }
    ]
});

// register guard to check if the route is accessible for the user
router.beforeEach((to, from, next) => {
    // if the required route is secured (not allowed for anonymous)
    if (!to.matched.some((record) => record.meta.isAnonymous)) {
        // is logged in
        if (vuexStore.getters.isLoggedIn) {
            next();
        } else {
            next("/login");
        }
    } else {
        // in an anonymous route
        next();
    }
});

// register guard to load the user data if the user is logged in and the
router.beforeEach(async (to, from, next) => {
    // test to determine if the data have been loaded (id of profile is undefined when data have not been loaded)
    if (
        !to.matched.some((record) => record.meta.isAnonymous) &&
        (vuexStore.state as any).profile.profile.id === undefined
    ) {
        const loadingComponent = Vue.prototype.$loading.open({
            container: null
        });
        try {
            await vuexStore.dispatch("loadUserData");
            next();
        } catch (err) {
            Vue.prototype.$toast.open({
                message:
                    "Une erreur est survenue lors du chargement de vos données. Veuillez réessayer.",
                type: "is-danger"
            });
            next("/login");
        } finally {
            loadingComponent.close();
        }
    } else {
        next();
    }
});

// register guard to load the stats data if the user accesses a stats page
router.beforeEach(async (to: Route, from: Route, next: any) => {
    if (
        to.meta.isStats &&
        (vuexStore.state as any).stats.eventId != to.params.id
    ) {
        const loadingComponent = Vue.prototype.$loading.open({
            container: null
        });
        try {
            await vuexStore.dispatch("stats/loadStats", to.params.id);
            next();
        } catch (err) {
            console.log(err);
            Vue.prototype.$toast.open({
                message:
                    "Une erreur est survenue lors du chargement des statistiques. Veuillez réessayer.",
                type: "is-danger"
            });
            next(false);
        } finally {
            loadingComponent.close();
        }
    } else {
        next();
    }
});

export default router;
