import Product from "@/models/Product";
import Profile from "@/models/Profile";
import { GetterTree } from "vuex";
import { ProductState } from ".";
import { RootState } from "../store";

export const getters: GetterTree<ProductState, RootState> = {
  findById(state) {
    return (id: number) => state.products[id];
  },

  findUserPlan(state, getter, rootState, rootGetters): Product {
    const profile: Profile = (rootState as any).profile.profile;
    return state.products[profile.plan];
  }
};
