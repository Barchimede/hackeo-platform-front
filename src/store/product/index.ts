import Product from "@/models/Product";
import { Module } from "vuex";
import { RootState } from "../store";
import { action } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";

export interface ProductState {
  products: Product[];
}

const defaultState: ProductState = {
  products: []
};

export const product: Module<ProductState, RootState> = {
  namespaced: true,
  state: defaultState,
  mutations,
  actions: action,
  getters
};
