import Product from "@/models/Product";
import { MutationTree } from "vuex";
import { ProductState } from ".";

export const mutations: MutationTree<ProductState> = {
  load(state, payload: Product[]) {
    state.products = payload;
  }
};
