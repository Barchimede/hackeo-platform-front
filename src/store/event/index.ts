import Event from "@/models/Event";
import Journey from "@/models/Journey";
import JourneyLine from "@/models/JourneyLine";
import Media from "@/models/Media";
import Setting from "@/models/Setting";
import Team from "@/models/Team";
import Visitor from "@/models/Visitor";
import VisitorToken from "@/models/VisitorToken";
import { Module } from "vuex";
import { RootState } from "../store";
import { action } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";

export interface EventState {
  events: Event[];
  teams: Team[];
  journeys: Journey[];
  journeyLines: JourneyLine[];
  visitors: Visitor[];
  visitorTokens: VisitorToken[];
  settings: Setting[];
  medias: Media[];
}

const defaultState: EventState = {
  events: [],
  teams: [],
  journeys: [],
  journeyLines: [],
  visitors: [],
  visitorTokens: [],
  settings: [],
  medias: []
};

export const event: Module<EventState, RootState> = {
  namespaced: true,
  state: defaultState,
  mutations,
  getters,
  actions: action
};
