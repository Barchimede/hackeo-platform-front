import Event, { EventStatus } from "@/models/Event";
import HackeoStatsResponse from "@/models/HackeoStatsReponse";
import Story from "@/models/Story";
import VisitorToken from "@/models/VisitorToken";
import hackeoApi from "@/services/hackeo-api";
import { ActionTree } from "vuex";
import { EventState } from ".";
import { RootState, store } from "../store";

export const action: ActionTree<EventState, RootState> = {
    async generateToken({ commit }, payload: number) {
        const token: any = await hackeoApi.generateVisitorToken(payload);
        const visitorToken: VisitorToken = {
            id: token.id, // not important here
            token: token.token,
            lastUse: undefined,
            deviceId: "",
            extraInfo: "",
            bonus: 0,
            useCount: 0,
            event: payload,
            isReusable: false
        };

        commit("insertToken", visitorToken);
        return visitorToken;
    },

    async acceptPictures(
        { commit },
        payload: { eventId: number; pictureIds: number[] }
    ) {
        await hackeoApi.acceptPictures(payload.eventId, payload.pictureIds);
        commit("acceptPictures", payload.pictureIds);
    },

    async refusePictures(
        { commit },
        payload: { eventId: number; pictureIds: number[] }
    ) {
        await hackeoApi.refusePictures(payload.eventId, payload.pictureIds);
        commit("refusePictures", payload);
    },

    async putEventTest({ commit, state }, payload: number) {
        await hackeoApi.putEventTest(payload);
        commit("putEventTest", payload);
        Object.values(state.journeys)
            .filter((journey) => journey.event === payload)
            .forEach((journey) =>
                commit("waypoint/resetWaypoints", journey, { root: true })
            );
    },

    async putEventOnline({ commit, state }, event: Event) {
        if (!event.story) {
            throw new Error("Il n'y a pas de scénario prévu pour l'événement.");
        }

        // if there is no team for a manageable event where the team gameplay is enabled
        if (
            event.status !== EventStatus.notManageable &&
            event.isTeamEnabled &&
            event.teams.length === 0
        ) {
            throw new Error(
                "La gestion d'équipe est activée mais il n'y a pas d'équipe enregistrée pour cet événement."
            );
        }
        const story: Story = (store.state as any).story.stories[event.story];
        const storyWaypointCount = story.waypointContents.length;

        if (
            event.status !== EventStatus.notManageable &&
            storyWaypointCount > event.markerCount
        ) {
            throw new Error(
                "Le nombre de balises du scénario '" +
                    story.name +
                    "' est trop important (" +
                    storyWaypointCount +
                    ") par rapport à celui de votre événement (" +
                    event.markerCount +
                    ")."
            );
        }
        try {
            await hackeoApi.putEventOnline(event.id);
        } catch (err) {
            const message = err.message as string;
            // Catch the API error for Plan not paid
            if (
                message.includes(
                    "The event cannot be put online if the customer has not paid his main order."
                )
            ) {
                throw new Error(
                    "L'événement ne peut pas être mis en ligne si la commande principale n'est pas réglée."
                );
            }

            // catch the API error for wrong waypoint order
            if (
                message.includes(
                    "Vous ne pouvez pas avoir un parcours avec une interruption de séquence"
                )
            ) {
                throw new Error(
                    // tslint:disable-next-line:max-line-length
                    "Vous ne pouvez pas avoir un parcours avec une interruption de séquence dans les balises. Veuillez vérifier l'ordre."
                );
            }
        }

        commit("putEventOnline", {
            eventId: event.id,
            planEnd: (store.state as any).profile.profile.planEnd.date
        });

        // reset the testing data of the event
        Object.values(state.journeys)
            .filter((journey) => journey.event === event.id)
            .forEach((journey) =>
                commit("waypoint/resetWaypoints", journey, { root: true })
            );
    },

    async updateEvent({ commit }, payload: Event) {
        const event: Event = await hackeoApi.putEvent(payload);
        commit("updateEvent", event);
    }
};
