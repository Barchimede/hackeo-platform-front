import Event, { EventDurationType, EventStatus } from "@/models/Event";
import Journey from "@/models/Journey";
import JourneyLine from "@/models/JourneyLine";
import Media from "@/models/Media";
import VisitorToken from "@/models/VisitorToken";
import { GetterTree } from "vuex";
import { EventState } from ".";
import { RootState } from "../store";

export const getters: GetterTree<EventState, RootState> = {
    journeyLineCount(state, rootState): number {
        return state.journeyLines.length;
    },

    findVisitorOf(state): any {
        return (journeyLine: JourneyLine) =>
            state.visitors[journeyLine.visitor];
    },

    findEventOfJourney(state) {
        return (journeyId: number) =>
            state.journeys[journeyId]
                ? state.events[state.journeys[journeyId].event]
                : undefined;
    },

    findEvents(state) {
        return state.events;
    },

    findNonArchivedEvents(state) {
        return Object.values(state.events)
            .filter((event) => event.eventEnd)
            .sort(
                (a: Event, b: Event) =>
                    new Date(b.eventEnd.date).getTime() -
                    new Date(a.eventEnd.date).getTime()
            );
    },

    findEventById(state) {
        return (id: number) => state.events[id];
    },

    findVisitorCountOfEvent(state) {
        return (event: Event) => {
            return state.events[event.id].visitorCount;
        };
    },

    findVisitorCount(state, getter, rootState, rootGetters): number {
        let sum: number = 0;
        for (const event of Object.values(state.events)) {
            sum += parseInt(event.visitorCount as any, 10); // parse int because of the string bug
        }
        return sum;
    },

    findVisitTimeOfEvent(state): (event: Event) => any {
        return (event: Event) => {
            // parse to days/hours/minutes/seconds
            const days = event.totalVisitTime / (24 * 3600);
            const hours = (days - Math.floor(days)) * 24;
            const minutes = (hours - Math.floor(hours)) * 60;
            const seconds = (minutes - Math.floor(minutes)) * 60;

            return {
                day: Math.round(days),
                hour: Math.round(hours),
                min: Math.round(minutes),
                sec: Math.round(seconds)
            };
        };
    },

    findVisitorTokensByEvent(state) {
        return (event: Event) =>
            Object.values(state.visitorTokens).filter(
                (visitorToken: VisitorToken) => visitorToken.event === event.id
            );
    },

    findComments(state, getter, rootState, rootGetters) {
        return {};
    },

    findAppSettings(state) {
        return Object.values(state.settings).filter(
            (setting) => setting.customer && setting.customer
        );
    },

    findVisitorsStatsOfEvent(state, getter, rootState, rootGetters) {
        // retrieve the focused event ID in stats page
        const eventId = (rootState as any).stats.eventId;
        const event: Event = state.events[eventId];

        let totalTime = 0; // total time for all visitors
        let finishVisitorCount = 0;
        const visitorsData: any[] = [];
        const journeyLines: JourneyLine[] = event.journeys
            .map((journeyId) => state.journeys[journeyId])
            .map((journey) =>
                journey.journeyLines.map(
                    (journeyLineId) =>
                        (rootState as any).stats.journeyLines[journeyLineId]
                )
            )
            .reduce(
                (previous, current) => (current = [...current, ...previous])
            );

        for (const journeyLine of journeyLines) {
            totalTime += journeyLine.currentGameTime;
            visitorsData.push({
                name: (rootState as any).stats.visitors[journeyLine.visitor]
                    .username,
                email: (rootState as any).stats.visitors[journeyLine.visitor]
                    .email,
                gameTime: journeyLine.currentGameTime,
                isFinished: journeyLine.isFinished,
                team: "Coucou"
            });
            // increment the finish visitor count if the player has finished the game
            finishVisitorCount = journeyLine.isFinished
                ? finishVisitorCount + 1
                : finishVisitorCount;
        }

        return {
            visitorsData,
            averageTime: Math.round(totalTime / journeyLines.length),
            finishVisitorCount,
            totalVisitors: journeyLines.length
        };
    }
};
