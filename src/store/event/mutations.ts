import Event, { EventDurationType, EventStatus } from "@/models/Event";
import VisitorToken from "@/models/VisitorToken";
import { MutationTree } from "vuex";
import { EventState } from ".";

export const mutations: MutationTree<EventState> = {
    load(state, payload: EventState) {
        state.events = payload.events;
        state.teams = payload.teams;
        state.journeys = payload.journeys;
        state.journeyLines = payload.journeyLines;
        state.visitors = payload.visitors;
        state.visitorTokens = payload.visitorTokens;
        state.settings = payload.settings;
        state.medias = payload.medias;
    },

    insertToken(state, payload: VisitorToken) {
        const visitorTokens = { ...state.visitorTokens };
        visitorTokens[payload.id] = payload;
        state.visitorTokens = visitorTokens;

        const events = { ...state.events };
        events[payload.event].visitorTokens.push(payload.id);
        state.events = { ...events };
    },

    acceptPictures(state, payload: number[]) {
        const medias = { ...state.medias };
        payload.forEach((id) => (medias[id].isValidated = true));
        state.medias = { ...medias };
    },

    refusePictures(state, payload: { eventId: number; pictureIds: number[] }) {
        const events = { ...state.events };
        state.events = { ...events };
    },

    updateEvent(state, payload: Event) {
        const events = { ...state.events };
        events[payload.id] = payload;
        state.events = { ...events };
    },

    putEventTest(state, payload: number) {
        const events = { ...state.events };
        events[payload].status = EventStatus.testing;
        events[payload].lastUpdate.date = new Date();
        state.events = events;
    },

    putEventOnline(state, payload: { eventId: number; planEnd: Date }) {
        const events = { ...state.events };
        const event: Event = events[payload.eventId];

        if (event.durationType === EventDurationType.punctual) {
            event.eventEnd.date = new Date(new Date().getTime() + 2592000000); // + 30 days
        } else {
            event.eventEnd.date = payload.planEnd;
        }
        event.status = EventStatus.manageable;
        event.eventStart.date = new Date();

        events[payload.eventId] = event;
        event.lastUpdate.date = new Date();
        state.events = events;
    }
};
