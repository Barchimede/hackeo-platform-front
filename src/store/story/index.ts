import Answer from "@/models/Answer";
import AnswerStat from "@/models/AnswerStat";
import Question from "@/models/Question";
import Story from "@/models/Story";
import WaypointContent from "@/models/WaypointContent";
import { Module } from "vuex";
import { RootState } from "../store";
import { action } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";

export interface StoryState {
  stories: Story[];
  waypointContents: WaypointContent[];
  questions: Question[];
  answers: Answer[];
  answerStats: AnswerStat[];
}

const defaultState: StoryState = {
  stories: [],
  waypointContents: [],
  questions: [],
  answers: [],
  answerStats: []
};

export const story: Module<StoryState, RootState> = {
  namespaced: true,
  state: defaultState,
  mutations,
  actions: action,
  getters
};
