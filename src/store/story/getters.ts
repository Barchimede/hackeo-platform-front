import Beacon from "@/models/Beacon";
import Event from "@/models/Event";
import Journey from "@/models/Journey";
import JourneyLine from "@/models/JourneyLine";
import Story from "@/models/Story";
import Team from "@/models/Team";
import Waypoint from "@/models/Waypoint";
import WaypointContent from "@/models/WaypointContent";
import { GetterTree } from "vuex";
import { StoryState } from ".";
import { RootState } from "../store";
import { waypoint } from "../waypoint";

export const getters: GetterTree<StoryState, RootState> = {
    findFeaturedStories(state, rootState): Story[] {
        return Object.values(state.stories)
            .filter((story: Story) => {
                return story.isFeatured && !story.isEditable;
            })
            .sort(
                (a: Story, b: Story) =>
                    new Date(a.lastModified.date).getTime() -
                    new Date(b.lastModified.date).getTime()
            );
    },

    findJourneyLinesOf(
        state,
        getter,
        rootState: RootState | any,
        rootGetters
    ): any {
        return (story: Story): JourneyLine[] => {
            // retrieve the events related to the specified story
            const events: Event[] = Object.values<Event>(
                rootState.event.events
            ).filter((event: Event) => event.story === story.id);

            // retrieve all journeys related to the events
            const journeys: Journey[] = [];
            for (const event of events) {
                journeys.concat(
                    event.journeys.map<Journey>(
                        (journeyId: number) =>
                            rootState.event.journeys[journeyId]
                    )
                );
            }

            // retrieve all the journey lines related to the journeys (so, related to the story, indirectly)
            const journeyLines: JourneyLine[] = [];
            for (const journey of journeys) {
                journeyLines.concat(
                    journey.journeyLines.map(
                        (lineId: number) => rootState.event.journeyLines[lineId]
                    )
                );
            }
            // return the average
            return journeyLines;
        };
    },

    findWaypointContentsOf(state, getter, rootState, rootGetters) {
        return (beacon: Beacon) => {
            const waypoints: Waypoint[] = rootGetters["waypoint/findAllOf"](
                beacon
            );

            return waypoints
                .map<WaypointContent>(
                    (value: Waypoint) =>
                        state.waypointContents[value.waypointContent]
                )
                .filter((v, i, a) => a.indexOf(v) === i); // last filter is used to guarantee unique values
        };
    }
};
