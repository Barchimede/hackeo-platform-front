import { MutationTree } from "vuex";
import { StoryState } from ".";

export const mutations: MutationTree<StoryState> = {
    load(state, payload: StoryState) {
        state.stories = payload.stories;
        state.waypointContents = payload.waypointContents;
        state.questions = payload.questions;
        state.answers = payload.answers;
        state.answerStats = payload.answerStats;
    }
};
