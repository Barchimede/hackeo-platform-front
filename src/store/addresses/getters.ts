import { GetterTree } from "vuex";
import { AddressState } from ".";
import { RootState } from "../store";

export const getters: GetterTree<AddressState, RootState> = {
  findById(state) {
    return (id: number) => state.addresses[id];
  }
};
