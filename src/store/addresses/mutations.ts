import Address from "@/models/Address";
import { MutationTree } from "vuex";
import { AddressState } from ".";

export const mutations: MutationTree<AddressState> = {
  load(state, payload: Address[]) {
    state.addresses = payload;
  },
  update(state, payload: Address) {
    state.addresses[payload.id] = payload;
  }
};
