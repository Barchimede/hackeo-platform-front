import Address from "@/models/Address";
import hackeoApi from "@/services/hackeo-api";
import { ActionTree } from "vuex";
import { AddressState } from ".";
import { RootState } from "../store";

export const action: ActionTree<AddressState, RootState> = {
  async update({ commit }, payload: Address) {
    const data = await hackeoApi.putAddress(payload);
    const address: Address = {
      id: payload.id,
      addressOne: data.addressOne,
      addressTwo: data.addressTwo,
      city: data.city,
      cp: data.cp,
      lat: data.lat,
      lng: data.lng,
      alt: data.alt
    };
    commit("update", address);
    return address;
  }
};
