import Address from "@/models/Address";
import { Module } from "vuex";
import { RootState } from "../store";
import { action } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";

export interface AddressState {
  addresses: Address[];
}

const defaultState: AddressState = {
  addresses: []
};

export const address: Module<AddressState, RootState> = {
  namespaced: true,
  state: defaultState,
  mutations,
  actions: action,
  getters
};
