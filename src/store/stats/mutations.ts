import Event from "@/models/Event";
import HackeoStatsResponse from "@/models/HackeoStatsReponse";
import { MutationTree } from "vuex";
import { StatsState } from ".";

export const mutations: MutationTree<StatsState> = {
    load(state, payload: { data: HackeoStatsResponse; event: Event }) {
        state.answerStats = payload.data.answerStats;
        state.journeyLines = payload.data.journeyLines;
        state.medias = payload.data.medias;
        state.visitors = payload.data.visitors;
        state.eventId = payload.event.id;
        state.endDate = payload.event.eventEnd.date;
        state.startDate = payload.event.eventStart.date;
    },

    changeDate(state, payload: { startDate: Date; endDate: Date }) {
        state.endDate = payload.endDate;
        state.startDate = payload.startDate;
    }
};
