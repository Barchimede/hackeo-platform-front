import Event from "@/models/Event";
import HackeoStatsResponse from "@/models/HackeoStatsReponse";
import hackeoApi from "@/services/hackeo-api";
import { ActionTree } from "vuex";
import { StatsState } from ".";
import { RootState } from "../store";

export const action: ActionTree<StatsState, RootState> = {
    async loadStats({ commit, state, rootGetters }, eventId: number) {
        // do nothing if the wanted event is not changed
        if (eventId === state.eventId) {
            return;
        }
        const event: Event = rootGetters["event/findEventById"](eventId);

        if (!event) {
            throw new Error();
        }

        const data: HackeoStatsResponse = await hackeoApi.getEventStatsData(
            eventId
        );

        console.log(data);

        commit("load", { data, event });
    }
};
