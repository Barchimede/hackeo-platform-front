import AnswerStat from "@/models/AnswerStat";
import JourneyLine from "@/models/JourneyLine";
import Media from "@/models/Media";
import Visitor from "@/models/Visitor";
import { Module } from "vuex";
import { RootState } from "../store";
import { action } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";

export interface StatsState {
    answerStats: { [key: number]: AnswerStat };
    medias: { [key: number]: Media };
    journeyLines: { [key: number]: JourneyLine };
    visitors: { [key: number]: Visitor };
    eventId: number; // the id of the event related to the stats
    startDate: Date | undefined;
    endDate: Date | undefined;
}

const defaultState: StatsState = {
    answerStats: {},
    medias: {},
    journeyLines: {},
    visitors: {},
    eventId: -1,
    startDate: undefined,
    endDate: undefined
};

export const stats: Module<StatsState, RootState> = {
    namespaced: true,
    state: defaultState,
    mutations,
    getters,
    actions: action
};
