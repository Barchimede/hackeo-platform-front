import JourneyLine from "@/models/JourneyLine";
import Media from "@/models/Media";
import { GetterTree } from "vuex";
import { StatsState } from ".";
import { RootState } from "../store";

export const getters: GetterTree<StatsState, RootState> = {
    findFeedbacks(state, getter, rootState, rootGetters) {
        const feedbacks: any[] = [];
        const ratings = [0, 0, 0, 0, 0];
        let total = 0;
        let sum = 0;

        Object.values(state.journeyLines)
            .sort((a, b) => b.rating - a.rating) // sort by rating (DESC)
            .forEach((journeyLine: JourneyLine) => {
                feedbacks.push({
                    name: state.visitors[journeyLine.visitor].username,
                    journey: (rootState as any).event.journeys[
                        journeyLine.journey
                    ].name,
                    comment: journeyLine.comment,
                    rating: journeyLine.rating,
                    gameTime: Math.round(journeyLine.currentGameTime / 60)
                });
                if (journeyLine.rating !== 0) {
                    ratings[journeyLine.rating - 1]++;
                    total++;
                }
                sum += journeyLine.rating;
            });
        return {
            feedbacks,
            ratings,
            total,
            averageRate: total !== 0 ? Math.round(sum / total) : 0
        };
    },

    findFeedbackRates(state, getter, rootState, rootGetters) {
        const ratings = [0, 0, 0, 0, 0];
        let total = 0;
        let sum = 0;

        Object.values(state.journeyLines)
            .sort((a, b) => b.rating - a.rating) // sort by rating (DESC)
            .forEach((journeyLine: JourneyLine) => {
                if (journeyLine.rating !== 0) {
                    ratings[journeyLine.rating - 1]++;
                    total++;
                }
                sum += journeyLine.rating;
            });
        return {
            ratings,
            total,
            averageRate: total !== 0 ? Math.round(sum / total) : 0
        };
    },

    findMediaStats(state, getter, rootState, rootGetters) {
        const result: any[] = [];
        Object.values(state.medias).forEach((media: Media) => {
            result.push({
                webPath: media.webPath,
                date: media.uploadDate.date,
                visitor:
                    state.visitors[
                        state.journeyLines[media.journeyLine].visitor
                    ].username
            });
        });
        return result;
    }
};
