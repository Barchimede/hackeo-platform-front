import { HackeoResponse } from "@/models/HackeoResponse";
import hackeoApi from "@/services/hackeo-api";
import Vue from "vue";
import Vuex, { ActionTree, GetterTree, MutationTree, StoreOptions } from "vuex";
import { address } from "./addresses";
import { beacon } from "./beacon";
import { event } from "./event";
import { order } from "./order";
import { product } from "./product";
import { profile } from "./profile";
import { stats } from "./stats";
import { story } from "./story";
import { waypoint } from "./waypoint";

export function parseJwt(token: string) {
    const base64Url = token.split(".")[1];
    const base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(window.atob(base64));
}

Vue.use(Vuex);

export interface RootState {
    token: string;
}

const rootAction: ActionTree<RootState, RootState> = {
    async loadUserData({ commit }) {
        try {
            const data: HackeoResponse = await hackeoApi.getUserData();
            // commits all the loader's mutation
            const rootMode = { root: true };
            commit("product/load", data.products, rootMode);
            commit("profile/load", data.profile, rootMode);
            commit(
                "story/load",
                {
                    stories: data.stories,
                    waypointContents: data.waypointContents,
                    questions: data.questions,
                    answers: data.answers,
                    answerStats: data.answerStats
                },
                rootMode
            );
            commit(
                "event/load",
                {
                    events: data.events,
                    teams: data.teams,
                    journeys: data.journeys,
                    journeyLines: data.journeyLines,
                    visitors: data.visitors,
                    visitorTokens: data.visitorTokens,
                    settings: data.settings,
                    medias: data.medias
                },
                rootMode
            );
            commit("beacon/load", data.beacons, rootMode);
            commit(
                "order/load",
                {
                    orderings: data.orderings,
                    orderLines: data.orderLines
                },
                rootMode
            );
            commit("waypoint/load", data.waypoints, rootMode);
            commit("address/load", data.addresses, rootMode);
        } catch (ex) {
            throw ex;
        }
    },

    async login({ commit }, payload: { email: string; password: string }) {
        try {
            const token: string = await hackeoApi.login(
                payload.email,
                payload.password
            );

            localStorage.setItem("token", token);
            commit("auth_success", token);
        } catch (ex) {
            localStorage.removeItem("token");
            commit("logout");
            throw ex;
        }
    },
    logout({ commit }) {
        localStorage.removeItem("token");
        commit("logout");
        hackeoApi.clearHeader();
    }
};

const rootGetter: GetterTree<RootState, RootState> = {
    isLoggedIn(state: RootState): boolean {
        return (
            !!state.token &&
            new Date(parseJwt(state.token).exp * 1000) > new Date()
        );
    }
};

const rootMutations: MutationTree<RootState> = {
    auth_success(state, token: string) {
        state.token = token;
    },
    logout(state) {
        state.token = "";
    }
};

export const store: StoreOptions<RootState> = {
    state: {
        token: localStorage.getItem("token") || ""
    },
    modules: {
        profile,
        product,
        story,
        event,
        beacon,
        order,
        waypoint,
        address,
        stats
    },
    actions: rootAction,
    getters: rootGetter,
    mutations: rootMutations
};

const vuexStore = new Vuex.Store<RootState>(store);
export default vuexStore;
