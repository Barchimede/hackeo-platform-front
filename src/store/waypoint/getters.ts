import Beacon from "@/models/Beacon";
import Waypoint from "@/models/Waypoint";
import { GetterTree } from "vuex";
import { WaypointState } from ".";
import { RootState } from "../store";

export const getters: GetterTree<WaypointState, RootState> = {
    findAllOf(state) {
        return (beacon: Beacon) =>
            beacon.waypoints.map<Waypoint>(
                (waypointId: number) => state.waypoints[waypointId]
            );
    }
};
