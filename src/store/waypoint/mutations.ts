import Journey from "@/models/Journey";
import Waypoint from "@/models/Waypoint";
import { MutationTree } from "vuex";
import { WaypointState } from ".";

export const mutations: MutationTree<WaypointState> = {
    load(state, payload: Waypoint[]) {
        state.waypoints = payload;
    },

    resetWaypoints(state, payload: Journey) {
        const waypoints = { ...state.waypoints };

        for (const waypointId in waypoints) {
            if (waypoints[waypointId].journey === payload.id) {
                waypoints[waypointId].lastVisitTime = undefined;
                waypoints[waypointId].visitCount = 0;
            }
        }

        state.waypoints = waypoints;
    }
};
