import Waypoint from "@/models/Waypoint";
import { Module } from "vuex";
import { RootState } from "../store";
import { action } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";

export interface WaypointState {
  waypoints: Waypoint[];
}

const defaultState: WaypointState = {
  waypoints: []
};

export const waypoint: Module<WaypointState, RootState> = {
  namespaced: true,
  state: defaultState,
  mutations,
  getters,
  actions: action
};
