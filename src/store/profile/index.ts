import Profile from "@/models/Profile";
import { Module } from "vuex";
import { RootState } from "../store";
import { action } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";

export interface ProfileState {
    profile: Profile;
}

const defaultState: ProfileState = {
    profile: {} as Profile
};

export const profile: Module<ProfileState, RootState> = {
    namespaced: true,
    state: defaultState,
    mutations,
    actions: action,
    getters
};
