import Profile from "@/models/Profile";
import { MutationTree } from "vuex";
import { ProfileState } from ".";

export const mutations: MutationTree<ProfileState> = {
  load(state, payload: Profile) {
    state.profile = payload;
  }
};
