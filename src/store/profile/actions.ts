import Address from "@/models/Address";
import Profile from "@/models/Profile";
import hackeoApi from "@/services/hackeo-api";
import { ActionTree } from "vuex";
import { ProfileState } from ".";
import { RootState } from "../store";

export const action: ActionTree<ProfileState, RootState> = {
  async updateProfile(
    { commit },
    payload: { profile: Profile; companyAddress: Address }
  ) {
    const data = await hackeoApi.putProfile(
      payload.profile,
      payload.companyAddress
    );
    commit("address/update", payload.companyAddress, { root: true });
    commit("load", payload.profile);
  }
};
