import Profile from "@/models/Profile";
import { GetterTree } from "vuex";
import { ProfileState } from ".";
import { RootState } from "../store";

export const getters: GetterTree<ProfileState, RootState> = {
  getter(state, rootGetters): Profile {
    return state.profile as Profile;
  }
};
