import Beacon from "@/models/Beacon";
import { Module } from "vuex";
import { RootState } from "../store";
import { action } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";

export interface BeaconState {
  beacons: Beacon[];
}

const defaultState: BeaconState = {
  beacons: []
};

export const beacon: Module<BeaconState, RootState> = {
  namespaced: true,
  state: defaultState,
  mutations,
  actions: action,
  getters
};
