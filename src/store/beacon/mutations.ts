import Beacon from "@/models/Beacon";
import { MutationTree } from "vuex";
import { BeaconState } from ".";

export const mutations: MutationTree<BeaconState> = {
  load(state, payload: Beacon[]) {
    state.beacons = payload;
  }
};
