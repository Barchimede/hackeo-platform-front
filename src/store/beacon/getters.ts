import Beacon from "@/models/Beacon";
import { GetterTree } from "vuex";
import { BeaconState } from ".";
import { RootState } from "../store";

export const getters: GetterTree<BeaconState, RootState> = {
  findAll(state, rootGetters): Beacon[] {
    return Object.values(state.beacons);
  },

  beaconCount(state, rootGetters): number {
    return Object.keys(state.beacons).length;
  }
};
