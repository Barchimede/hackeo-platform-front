import Ordering from "@/models/Ordering";
import Product from "@/models/Product";
import { GetterTree } from "vuex";
import { OrderState } from ".";
import { RootState } from "../store";

export const getters: GetterTree<OrderState, RootState> = {
    findAllOrders(state): Ordering[] {
        return state.orders;
    },

    findBeaconCountOfOrder(
        state,
        getter,
        rootState,
        rootGetters
    ): (order: Ordering) => number {
        return (order: Ordering) => {
            const findProduct: (id: number) => Product =
                rootGetters["product/findById"];
            return order.orderLines
                .map(
                    (orderLineId: number) =>
                        state.orderLines[orderLineId].quantity *
                        findProduct(state.orderLines[orderLineId].product)
                            .beaconCount
                )
                .reduce((previous, current) => (current += previous));
        };
    },

    findTotalOfOrder(state) {
        return (order: Ordering) =>
            order.orderLines
                .map(
                    (orderLineId: number) => state.orderLines[orderLineId].price
                )
                .reduce(
                    (previous: number, current: number) => (current += previous)
                );
    },

    findLineInfo(state, getter, rootState, rootGetters) {
        return (order: Ordering) => {
            const findProduct: (id: number) => Product =
                rootGetters["product/findById"];

            return order.orderLines.map((orderLineId: number) => {
                const orderLine = state.orderLines[orderLineId];
                const product: Product = findProduct(orderLine.product);
                return {
                    id: orderLine.id,
                    product: product.name,
                    quantity: orderLine.quantity,
                    price: orderLine.price
                };
            });
        };
    }
};
