import Ordering from "@/models/Ordering";
import OrderLine from "@/models/OrderLine";
import { Module } from "vuex";
import { RootState } from "../store";
import { action } from "./actions";
import { getters } from "./getters";
import { mutations } from "./mutations";

export interface OrderState {
  orders: Ordering[];
  orderLines: OrderLine[];
}

const defaultState: OrderState = {
  orders: [],
  orderLines: []
};

export const order: Module<OrderState, RootState> = {
  namespaced: true,
  state: defaultState,
  mutations,
  getters,
  actions: action
};
