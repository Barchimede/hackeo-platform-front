import Ordering from "@/models/Ordering";
import OrderLine from "@/models/OrderLine";
import { MutationTree } from "vuex";
import { OrderState } from ".";

export const mutations: MutationTree<OrderState> = {
  load(state, payload: { orderings: Ordering[]; orderLines: OrderLine[] }) {
    state.orders = payload.orderings;
    state.orderLines = payload.orderLines;
  }
};
