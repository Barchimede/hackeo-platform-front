import DateInfo from "./DateInfo";

export default interface Waypoint {
    id: number;
    isLocated: boolean;
    visitCount: number;
    lastVisitTime: DateInfo|undefined;
    isPublished: boolean;

    beacon: number;
    address: number;
    answerStats: number[];
    waypointContent: number;
    journey: number;
}
