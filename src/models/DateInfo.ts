export default interface DateInfo {
  date: Date;
  timezone_type: string;
  timezone: string;
}
