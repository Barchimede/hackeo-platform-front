export default interface Question {
  id: number;
  content: string;
  successComment: string;
  failComment: string;
  attemptCount: number;
  answers: number[];
  waypointContent: number;
}
