export default interface AnswerStat {
  id: number;
  answerCount: number;

  waypoint: number;
  answer: number;
}
