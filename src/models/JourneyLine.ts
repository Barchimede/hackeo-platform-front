export default interface JourneyLine {
  id: number;
  visitCount: number;
  currentGameTime: number;
  leftGameTime: number;
  isFinished: boolean;
  level: number;
  score: number;
  kilometerWalked: number;
  scanCount: number;
  comment: string;
  rating: number;

  visitor: number;
  journey: number;
}
