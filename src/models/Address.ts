export default interface Address {
  id: number;
  addressOne: string;
  addressTwo: string;
  city: string;
  cp: string;
  lat: number;
  lng: number;
  alt: number;
}
