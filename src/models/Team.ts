export default interface Team {
  id: number;
  name: string;
  description: string;
  visitorCount: number;
  scoreSum: number;
  isDefault: boolean;

  customLogo: string;

  story: number;
  event: number;
}
