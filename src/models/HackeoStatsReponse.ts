import AnswerStat from "./AnswerStat";
import JourneyLine from "./JourneyLine";
import Media from "./Media";
import Visitor from "./Visitor";

export default interface HackeoStatsResponse {
    answerStats: { [key: number]: AnswerStat };
    medias: { [key: number]: Media };
    journeyLines: { [key: number]: JourneyLine };
    visitors: { [key: number]: Visitor };
}
