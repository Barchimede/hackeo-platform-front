import DateInfo from "./DateInfo";

export enum MediaType {
  waypointContent = "Waypoint content",
  augmentedReality = "Augmented reality",
  augmentedRealityExtra = "Augmented reality extra",
  visitorUpload = "Visitor upload",
  eventEndImage = "Event end image",
  eventLogo = "Event logo",
  eventMap = "Event map",
  storyLogo = "Story logo",
  augmentedRealityPoly = "Augmented reality poly",
  storyScreenshot = "Story screenshot",
  teamLogo = "Team logo"
}

export default interface Media {
  id: number;
  type: MediaType;
  description: string;
  webPath: string;
  journeyLine: number;
  event: number;
  story: number;
  team: number;
  diskUsage: number;
  uploadDate: DateInfo;
  isValidated: boolean;
}
