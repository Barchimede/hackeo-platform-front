export default interface Answer {
  id: number;
  content: string;
  isCorrect: boolean;
  score: number;
  comment: string;
  question: number;
}
