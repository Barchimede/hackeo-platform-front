export enum WaypointContentType {
  question = "Question",
  image = "Image",
  sound = "Sound",
  video = "Video",
  text = "Text",
  link = "Link",
  digicode = "Digicode"
}

export enum WaypointContentTriggerMode {
  onlyWPC = "Only waypoint content",
  onlyNotif = "Only notification",
  both = "Notification then waypoint content"
}

export enum WaypointContentProximityType {
  indoorMarker = "Indoor marker",
  beaconDistance = "Beacon distance",
  beaconLight = "Beacon light",
  beaconTemperature = "Beacon temperature"
}

export default interface WaypointContent {
  id: number;
  name: string;
  description: string;
  clue: string;
  type: WaypointContentType;
  number: number;
  isDraft: boolean;
  uploadPicture: boolean;
  proximityType: WaypointContentProximityType;
  triggerMode: WaypointContentTriggerMode;
  triggerMinValue: number;
  triggerMaxValue: number;

  medias: string[];
  arMedias: string[];
  markerTriggers: string[];
  notificationContent: string;

  questions: number[];
  story: number;
}
