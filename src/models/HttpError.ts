export default class HttpError extends Error {
  public code: number;
  public resp: any;

  constructor(code: number, message: string | any, resp: any) {
    super(message);
    this.code = code;
    this.resp = resp;
  }
}

export const errorToast = (message: string) => {
  return {
    duration: 5000,
    message,
    position: "is-top-right",
    queue: false,
    type: "is-danger"
  };
};
