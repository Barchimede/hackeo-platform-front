export default interface OrderLine {
  id: number;
  quantity: number;
  price: number;

  ordering: number;
  product: number;
}
