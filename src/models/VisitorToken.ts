import DateInfo from "./DateInfo";

export default interface VisitorToken {
  id: number;
  token: string;
  lastUse: DateInfo|undefined;
  deviceId: string;
  isReusable: boolean;
  useCount: number;
  bonus: number;
  extraInfo: string;

  event: number;
}
