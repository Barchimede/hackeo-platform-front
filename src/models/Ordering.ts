import DateInfo from "./DateInfo";

export enum OrderingType {
  normal = "Normal",
  demo = "Demo",
  test = "Test"
}

export enum OrderingStatus {
  draft = "Draft",
  waitingValidation = "Waiting validation",
  waitingPayment = "Waiting payment",
  finished = "Finished",
  archived = "Archived"
}

export default interface Ordering {
  id: number;
  orderingDate: DateInfo;
  updateDate: DateInfo;
  status: OrderingStatus;
  serialNumber: string;
  type: OrderingType;
  invoiceUrl: string;

  customer: number;
  orderLines: number[];
  deliveryAddress: number;
}
