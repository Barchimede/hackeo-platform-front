import DateInfo from "./DateInfo";

export enum EventStatus {
    archived = "Archived",
    notManageable = "Not manageable",
    manageable = "Manageable",
    testing = "Testing"
}

export enum EventDurationType {
    punctual = "Punctual",
    Recurrent = "Recurrent",
    demo = "Demo"
}

export default interface Event {
    id: number;
    name: string;
    status: EventStatus;
    durationType: EventDurationType;
    token: string;
    createdAt: DateInfo;
    eventStart: DateInfo;
    eventEnd: DateInfo;
    lastUpdate: DateInfo;
    imgModeration: boolean;
    canUploadImg: boolean;
    scanCount: number;
    beaconPopupDistance: number;
    isTeamEnabled: boolean;
    locationRadius: number;
    interfaceColor: string;
    menuTypoColor: string;
    markerCount: number;
    journeyCount: number;
    isBeaconEnabled: boolean;
    isScheduleEnabled: boolean;
    isCharged: boolean;
    isEndScreenGameTimeEnabled: boolean;
    hasRandomizeWaypoints: boolean;
    hasInGameRestart: boolean;
    hasInGameRestartTimer: boolean;
    pictureCount: number;
    totalVisitTime: number;
    visitorCount: number;

    // paths of the medias
    customLogo: string;
    mapImage: string;
    interfaceMedias: string[];
    extraMarkerTriggers: string[];

    // reference of other nested states
    journeys: number[];
    customer: number;
    setting: number;
    story: number;
    teams: number[];
    visitorTokens: number[];
}
