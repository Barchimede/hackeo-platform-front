import DateInfo from "./DateInfo";

export default interface Setting {
    id: number;
    name: string;
    key: string;
    value: string;
    lastModified: DateInfo;

    customer: number;
}
