import DateInfo from "./DateInfo";

export default interface Visitor {
  id: number;
  emailCanBeUsed: boolean;
  osName: string;
  osVersion: string;
  registrationDate: DateInfo;
  email: string;
  username: string;
  deviceId: string;
}
