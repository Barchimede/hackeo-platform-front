import DateInfo from "./DateInfo";

export default interface Beacon {
  id: number;
  uuid: string;
  minor: number;
  major: number;
  deviceId: number;
  isReserved: boolean;
  isAssigned: boolean;
  customerAssignationDate: DateInfo;
  waypointAssignationDate: DateInfo;
  battery: number;
  logDate: DateInfo;

  journey: number;
  customer: number;
  waypoints: number[];
}
