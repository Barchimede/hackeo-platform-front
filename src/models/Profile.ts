import DateInfo from "./DateInfo";

/**
 * Describes the state of the logged in user
 */
export default interface Profile {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  isAdmin: boolean;
  companyName: string;
  lastUpdate: DateInfo;
  phoneNumber: string;
  planStart: DateInfo;
  planEnd: DateInfo;
  journeyWallet: number;
  diskUsage: number;

  // ids of the related objects
  companyAddress: number;
  plan: number;
  events: number[];
  beacons: number[];
  stories: number[];
  orderings: number[];
}
