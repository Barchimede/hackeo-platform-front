import DateInfo from "./DateInfo";

export enum StoryScoreType {
  money = "Money",
  energy = "Energy",
  leaf = "Leaf",
  bulb = "Bulb",
  diamond = "Diamond",
  star = "Star",
  custom = "Custom"
}

export enum StoryType {
  featured = 1,
  custom = 2
}

export default interface Story {
  id: number;
  name: string;
  canRescan: boolean;
  scoreType: StoryScoreType;
  eventCount: number;
  allowedGameTime: number;
  wpInSequence: boolean;
  visitorCount: number;
  lastModified: DateInfo;
  lastUse: boolean;
  isEditable: boolean;
  contentDir: string;
  descriptionText: string;
  startOneText: string;
  startTwoText: string;
  endOneText: string;
  endTwoText: string;
  outTimeText: string;
  enabled: boolean;
  type: StoryType;
  startDate: DateInfo;
  endDate: DateInfo;
  experienceTime: number;
  targetType: number;
  isFeatured: boolean;
  language: string;
  minimumAge: number;
  isPrintAndPlay: boolean;
  isHeadphonesRequired: boolean;
  averageDuration: number;
  minNumberOfPlayer: number;
  maxNumberOfPlayer: number;

  endImages: string[];
  screenshots: string[];
  customLogo: string;
  onboardMarker: string;
  loadingTexts: string[];

  owner: number;
  users: number[];
  waypointContents: number[];
}
