export default interface Product {
  id: number;
  name: string;
  description: string;
  price: number;
  isStackable: boolean;
  accountAccessDuration: number;
  waypointCount: number;
  beaconCount: number;
  userLimit: number;
  statAccess: number;
  graphicCustomization: number;
  isChargedJourney: boolean;
  arCardCount: number;
  isPack: boolean;
  key: string;
  isAdditional: boolean;
  isScheduleEnabled: boolean;
  isEnabled: boolean;
  contentAccessLevel: number;
  storeStoryCount: number;
  customStoryCount: number;
  journeyCount: number;
  maxStorage: number;
}
