export default interface Journey {
  id: number;
  name: string;
  isEnabled: boolean;
  scoreSum: number;
  visitorCount: number;

  mapImage: string;

  event: number;
  address: number;
  journeyLines: number[];
  beacons: number[];
}
